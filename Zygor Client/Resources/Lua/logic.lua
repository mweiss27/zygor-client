function MakeURL(vars)  -- do, user, pass...
	local url = "http://content.zygorguides.com/updater/packager.php"
	local first=true
	for k,v in pairs(vars) do
		url=url .. (first and "?" or "&")
		first=false
		url=url .. k .. "="
		local var=""
		for i=1,v:len() do
			local c=v:sub(i,i)
			if c:match("[0-9a-zA-Z]") then
				var=var..c
			else
				var=var.. ("%%%x"):format(c:byte(1))
			end
		end
		url=url .. var
	end
	return url
end

function __DO_HTTP(vars)  -- url, callback
	Http:DoHTTPRequest(vars['url'],vars['callback'], not not vars['fake'],vars['fake'] and vars['fake'][1],tostring(vars['fake'] and vars['fake'][2]))
end

function CheckLogin_A(vars) -- user,pass,callback
	__DO_HTTP{
		url=MakeURL{['do']="validate",['user']=vars['user'],['pass']=vars['pass']},
		callback=vars['callback'],
		fake={vars['user']~="error",vars['pass']=="correct" and "LOGIN OK" or "LOGIN ERR"}
	}
end

function GetQuery_A(vars)
	__DO_HTTP{
		url=MakeURL{['do']="query",['user']=vars['user'],['pass']=vars['pass']},
		callback=vars['callback'],
		fake={vars['user']=="elite",(vars['pass']=="correct" and "LOGIN OK" or "LOGIN ERR").."\n"..[[
INFO productfiles loaded
UPDATER_MD5 1dfd1bf411e5ff6f0676a9cf747114d6

VER 6.1.16524
PRODUCT zgv Zygor Guides Viewer addon
PRODUCT levl-a-trial Alliance Leveling & Loremaster (TRIAL)
PRODUCT levl-h-trial Horde Leveling & Loremaster (TRIAL)
PRODUCT daie-a-trial Alliance Dailies & Events (TRIAL)
PRODUCT daie-h-trial Horde Dailies & Events (TRIAL)
PRODUCT proa-a-trial Alliance Professions & Achievements (TRIAL)
PRODUCT proa-h-trial Horde Professions & Achievements (TRIAL)
PRODUCT titr-a-trial Alliance Titles & Reputations (TRIAL)
PRODUCT titr-h-trial Horde Titles & Reputations (TRIAL)
PRODUCT pets-a-trial Alliance Pets & Mounts (TRIAL)
PRODUCT pets-h-trial Horde Pets & Mounts (TRIAL)
PRODUCT dung-a-trial Alliance Dungeons & Gear (TRIAL)
PRODUCT dung-h-trial Horde Dungeons & Gear (TRIAL)
PRODUCTDIR ZygorGuidesViewer
CHG:
SMART INJECTION SYSTEM<br />
* When you enter a dungeon with a guide already loaded, then accept the pop up suggestion to switch to the guide for the dungeon and later leave the dungeon, the SIS will pop up asking if you wish to load your previously used guide again.<br />
<br />
GOLD GUIDE - GATHERING SECTION:<br />
* Fix: Easy/Expert switch now works correctly.<br />
* Fix: Guides now properly use item demand in display and tooltips.<br />
<br />
LEVELING<br />
[B] Fixed Leveling zones - Continued on vanilla zones: Fixed many steps that had multiple goals splitting them into separate steps. Fixed a few caves that were not working properly adding tags to correctly show the locations within these caves. Added enter and leave cave lines where needed. Added several stickysteps where needed.<br />
<br />
DAILIES<br />
[B] Added World Quests - Gloth, Ulgthax, Huge Mossgill Perch, Fragrant Dreamleaf, Mission: Felrage Destruction.<br />
<br />
PETS/MOUNTS<br />
[B] Updated Luminous Starseeker - Added missing displayid.<br />
[B] Updated Pets/Mounts - Updated syntax. updated some information<br />
<br />
PROFESSIONS<br />
[B] Updated Tailoring 1-600 Leveling Guide - Updated the Silk Cloth farming location to Death's Step in Eastern Plaguelands. Updated the Mageweave Cloth farming location to the Dunemaul Compound in Tanaris.<br />
<br />
ACHIEVEMENTS<br />
[A] Updated Intro to Husbandry<br />
[A] Added Explore Twilight Highlands - Added the guide with an optimized path for the Alliance.<br />
[H] Updated Explore Twilight Highlands - Finished the optimized path.<br />
[B] Added Cataclysm Explorer - Added the Cataclysm Explorer achievement<br />
[H] Added More Plots<br />
[B] Added Time for an Upgrade<br />
[B] Added Champions of Power<br />
<br />
TITLES<br />
[B] Added Vindictive Gladiator<br />
[B] Added Fearless Gladiator<br />

:CHG
END
]]
		}
	}
end

--[[
	STATES:

	[init]
	  |
	user/pass? ------.
	  |              |
	 else            |
	  v              |
	[login_dialog] <-+-.
	  |              | |
	entered     .----' |
	  v         v      |
	[login_pending]    |
	  |                |
	success? -- else --'
	  v
	[active]
--]]

STATE = "init"
STATE2 = ""
function SetState(state,state2)
	if state and state~=STATE then
		print("SetState: "..STATE.." -> "..state)
		STATE=state
		if STATE=='login_dialog' then
			print("UI: login dialog")
		elseif STATE=='login_pending' then
			print("UI: logging in...")
		elseif STATE=='active' then
			FetchUserStatus()
		end
	end
	if state2 and state2~=STATE2 then
		print("SetState 2: "..STATE.."/"..STATE2.." -> "..STATE.."/"..state2)
		STATE2=state2
	end
end

function MAIN()
	if CFG.user and CFG.pass then
		StartLogin(CFG.user,CFG.pass)   -- goes into state 'login_pending' and then into 'active' or back to 'login_dialog'
	else
		SetState('login_dialog','')
	end
end

function StartLogin(user,pass)
	SetState('login_pending')
	CFG.user = user
	CFG.pass = pass
	CheckLogin_A{
		user=CFG.user,
		pass=CFG.pass,
		callback=function(success,results)
			if not success then SetState('login_dialog','error_fail') return end
			if results=="LOGIN OK" then
				Log:Info("Login OK")
				SetState("active")
			elseif results=="LOGIN ERR" then
				Log:Info("Login error: "..tostring(results))
				SetState('login_dialog','error_loginbad')
			end
		end
	}
end

USERSTATUS={}

function FetchUserStatus()
	GetQuery_A{
		user=CFG.user,
		pass=CFG.pass,
		callback=function(success,results)
			if not success then USERSTATUS={msg='error_conn'} return end
			USERSTATUS={}
			results=results:gsub("\r","").."\n"
			local m1,m2,m3,m4
			local in_changelog
			for l in results:gmatch("([^\n]*)\n") do  repeat
				print(l)
				if in_changelog then
					if l==":CHG" then  in_changelog=false  break  end  --continue
					USERSTATUS.ChangeLog = (USERSTATUS.ChangeLog and USERSTATUS.ChangeLog.."\n" or "") .. l
					break --continue
				end
				m1=l:match("LOGIN ([OKERR]+)")  if m1 then
					USERSTATUS.LoginOK=(m2=="OK")
					break --continue
				end
				m1=l:match("VER ([0-9%.]+)")  if m1 then
					USERSTATUS.Version=m1
					break --continue
				end
				m1,m2=l:match("PRODUCT ([^ ]+) (.*)")  if m1 then
					USERSTATUS.Products = USERSTATUS.Products or {}
					table.insert(USERSTATUS.Products,{m1,m2})
				end
				m1=l:match("PRODUCTDIR (.+)")  if m1 then
					USERSTATUS.ProductDir=m1
					break --continue
				end
				if l=="CHG:" then in_changelog=true break end  --continue
				print("!?")
			until true  end
			blee()
		end,
	}
end

function SetUpCFG()
	CFG={}
	-- LoadConfig()
	CFG_DEFAULT = {
		['timerUpdateInterval']=20,
		['timerGoldInterval']=15,
		['timerHourlyInterval']=8,
		
		['user']="admin",
		['pass']="initialfail",
	}
	for k,v in pairs(CFG_DEFAULT) do if CFG[k]==nil then CFG[k]=v end end
end

SetUpCFG()

local timerUpdateLast=0
local timerGoldLast=0
local timerHourlyLast=0

TICKNUM=0

SIMULATION = {
	['STEP'] = 'wait_for_autologin',
	['wait_for_autologin'] = function()
		if STATE=='login_pending' then
			print("SIM: waiting for login result...")
		elseif STATE=='login_dialog' then
			print("SIM: login dialog detected, entering: error/error (should fail)")
			SIMULATION.STEP='wait_for_login_fail'
			StartLogin("error","error")
		else
			print("SIM: fail; STATE",STATE,"/",STATE2)
			SIMULATION.STEP=nil
		end
	end,
	['wait_for_login_fail'] = function()
		if STATE=='login_pending' then
			print("SIM: waiting for login result...")
		elseif STATE=='login_dialog' and STATE2=='error_fail' then
			print("SIM: login dialog detected with failure, entering: admin/deny (should deny)")
			SIMULATION.STEP='wait_for_login_deny'
			StartLogin("admin","deny")
		else
			print("SIM: fail; STATE",STATE,"/",STATE2)
			SIMULATION.STEP=nil
		end
	end,
	['wait_for_login_deny'] = function()
		if STATE=='login_pending' then
			print("SIM: waiting for login result...")
		elseif STATE=='login_dialog' and STATE2=='error_loginbad' then
			print("SIM: login dialog detected with denial, entering: elite/correct (should validate)")
			SIMULATION.STEP='wait_for_login_ok'
			StartLogin("elite","correct")
		else
			print("SIM: fail; STATE",STATE,"/",STATE2)
			SIMULATION.STEP=nil
		end
	end,
	['wait_for_login_ok'] = function()
		if STATE=='login_pending' then
			print("SIM: waiting for login OK...")
		elseif STATE=='active' then
			print("SIM: activated main UI, ending sim")
			SIMULATION.STEP=nil
		end
	end,
	Perform = function(self)
		if not self[self.STEP] then print("SIM: NO STEP",tostring(self.STEP)) SIMULATION=nil return end
		print("SIM: TICK #",TICKNUM,"  SIM STEP",self.STEP)
		self[self.STEP]()
	end
}

function OnTick() -- Called by API
	TICKNUM=TICKNUM+1
	
	if SIMULATION then SIMULATION:Perform() end
end


MAIN()