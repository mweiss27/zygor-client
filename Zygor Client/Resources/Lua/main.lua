local path="Resources/Lua/"

-- Warning, sender is of type ZCForm. Not a ZControl
local function saveClientPosition(sender, args)
	Log:Info("saveClientPosition")
	Prefs:Set(prefs.CLIENT_POSITION, Form:GetLocation())
end

Form:SetOnMoved(saveClientPosition)

dofile(path.."util.lua")
dofile(path.."topbar.lua")
dofile(path.."accountoverview.lua")
dofile(path.."logic.lua")