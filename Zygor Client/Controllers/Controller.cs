﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Threading;
using System.Timers;
using System.Windows.Forms;
using NLua;
using Zygor_Client.Util;
using NLua.Exceptions;
using System.Threading.Tasks;

namespace Zygor_Client.Controllers
{
    public class Controller
    {

        public ZCForm Form;
        private Preferences _prefs;

        public System.Timers.Timer BackgroundTimer { get; private set; }

		private System.Timers.Timer TickTimer;

		public Lua Lua { get; private set; }
		public object LuaLock = new object();
        public List<ComponentDragger> Draggers { get; } = new List<ComponentDragger>();

        public FormUtil FormUtil = FormUtil.Instance;
		public GeneralUtil Util { get; }
        public ControllerHttp Http { get; }
        public Log Log { get; }

        public static Controller Instance { get; private set; }

		internal void CallOnTick(object sender = null, ElapsedEventArgs args = null)
		{
			lock (LuaLock)
			{
				if (Lua is null) { Console.WriteLine("TICK: No Lua"); return; }
				LuaFunction OnTick = Lua.GetFunction("OnTick"); // why is null exception thrown here sometimes? should be safe!
				if (OnTick is null) { Console.WriteLine("TICK: No OnTick"); return; }
				try
				{
					OnTick.Call();
				}
				catch (LuaScriptException e)
				{
					MessageBox.Show(e.Message + "\nin\n" + e.Source, "Lua error");
				}
			}
		}

		public Controller()
        {

            Util = new GeneralUtil();
            Http = new ControllerHttp();
            Log = new Log();

            Form = new ZCForm();
            _prefs = new Preferences();
            BackgroundTimer = new System.Timers.Timer(1000 * 5);
			TickTimer = new System.Timers.Timer(1000);

            Instance = this;
        }

        public void Initialize()
        {

			InitLua();
			InitDragListener();

            var location = _prefs.Get(_prefs.CLIENT_POSITION, "").ToString();

            Form.StartPosition = FormStartPosition.Manual;
            if (!string.IsNullOrWhiteSpace(location))
            {
                var tokens = location.Split(',');
                Form.SetDesktopLocation(int.Parse(tokens[0]), int.Parse(tokens[1]));
            }
            else
                Form.Center();

        }

        private void InitDragListener()
        {
            Draggers.Clear();
            var topbar = FormUtil.FindZControlById(Form, "container", "topbar");
            if (topbar != null)
            {
                var topright = FormUtil.FindZControlById(topbar.Control, "topright");
                if (topright != null)
                    Draggers.Add(new ComponentDragger(topright.Control));

                var topleft = FormUtil.FindZControlById(topbar.Control, "topleft");
                if (topleft != null)
                    Draggers.Add(new ComponentDragger(topleft.Control));
            }
        }

		private void InitLua()
		{
			Lua = new Lua
			{
				// Setup global API access points
				["Form"] = Form,
				["Util"] = Util,
				["FormUtil"] = FormUtil,
				["Log"] = Log,
				["Prefs"] = _prefs,
				["Http"] = Http
			};

			Console.WriteLine("Running NLua " + NLua.Config.Consts.NLuaVersion);

			// Run our main script
			bool retry = false;
			do
			{
				try
				{
					Lua.DoFile("Resources/Lua/main.lua");
				}
				catch (LuaScriptException e)
				{
					if (MessageBox.Show(e.Message, "Lua error", MessageBoxButtons.RetryCancel) == DialogResult.Retry)
						retry = true;
					else
					{
						Application.Exit();
						retry = false;
					}
				}
			} while (retry);
		}

        public void CheckForUpdates(object sender=null, ElapsedEventArgs args = null)
        {
            if (Program.argflags["disableupdates"]) return;

            BackgroundTimer.Interval = Int32.MaxValue;
            var updated = Http.DownloadOutdatedResources(false);

            if (updated) {
                // We need to re-build the client
                ReloadUI();
            }

            BackgroundTimer.Interval = 5000;
        }

        public void Start()
        {
            BackgroundTimer.Elapsed += CheckForUpdates;
            BackgroundTimer.Start();

			TickTimer.Elapsed += CallOnTick;
			TickTimer.Start();
        }

        public void Stop()
        {
            Lua.Dispose();
        }

		public void ReloadUI()
		{
			var activeDraggers = Draggers.Any(d => d.Active);
			if (!activeDraggers)
			{
				lock (LuaLock)
				{
					Lua?.Dispose();
					Lua = null;
					Form.Invoke(new MethodInvoker(() =>
					{
						Form.Enabled = false;
						Form.SuspendLayout();
						Form.Controls.Clear();
						var root = Form.Build(FormUtil, "Resources/Views/main.xml");
						Form.Controls.Add(root);
						Form.ResumeLayout();
						Form.Enabled = true;
					}));
					GC.Collect();
					Initialize();
				}
			}
			else
				Debug.WriteLine("A ComponentDragger is active. Not rebuilding.");
		}

	}
}
