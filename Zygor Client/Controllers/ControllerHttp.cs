﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Security.Cryptography;
using System.Threading;
using System.Threading.Tasks;
using Zygor_Client.Util;

namespace Zygor_Client.Controllers
{
    public class ControllerHttp
    {

        /**
         * Downloads and returns the manifest as a string.
         * 
         * throws: WebException
         */
        private string RetrieveManifest()
        {
            var url = "https://content.zygorguides.com/client/manifest";

            using (var client = new WebClient())
                return client.DownloadString(url);
        }

        /**
         * Downloads a resource file and writes it to disk.
         * 
         * relativeFilePath: A path relative to the client directory. 
         * On the server: https://zygorguides.com/client/{relativePath}
         * On the users machine: ProgramData/ZGClient/{relativePath}
         * 
         * throws: WebException
         */
        public void DownloadFile(string relativeFilePath)
        {
            var url = $"https://content.zygorguides.com/client/{relativeFilePath}";

            var dirInfo = Directory.GetParent(relativeFilePath);
            Directory.CreateDirectory(dirInfo.FullName);

            using (var client = new WebClient())
                client.DownloadFile(url, relativeFilePath);
        }

        /**
         * Retrieves the manifest from the server, and compares all local resources by MD5.
         * Any missing our outdated resources are downloaded and written to file.
         * 
         * returns true if at least 1 file was successfully downloaded.
         */
        public bool DownloadOutdatedResources(bool manual)
        {
            try
            {
                var outdatedResources = GetOutdatedResources();
                var updated = 0;

                foreach (var line in outdatedResources)
                {
                    try
                    {
                        DownloadFile(line);
                        updated++;
                    }
                    catch (WebException)
                    {
                        Log.Instance.Fatal($"Failed to download {line}", manual);
                    }
                }

                return updated > 0;
            }
            catch (WebException)
            {
                Log.Instance.Fatal("Failed to retrieve the Resources manifest", manual);
            }
            return false;
        }

        private List<string> GetOutdatedResources()
        {
            var result = new List<string>();
            string manifest;
            try
            {
                manifest = RetrieveManifest();
            }
            catch (WebException e)
            {
                Log.Instance.Error("Failed to check for app updates. RetrieveManifest failed");
                Log.Instance.Error(e.Message);
                throw;
            }

            foreach (var line in manifest.Split('\n'))
            {
                var tokens = line.Split(',');
                if (tokens.Length == 2)
                {
                    var relativePath = tokens[0];
                    var md5 = tokens[1];

                    var update = !File.Exists(relativePath);
                    if (!update)
                        // It exists, let's compare md5s
                        using (var m = MD5.Create())
                        using (var stream = File.OpenRead(relativePath))
                        {
                            var servermd5 = BitConverter.ToString(m.ComputeHash(stream)).Replace("-", String.Empty);
                            if (md5.ToLower().Trim() != servermd5.ToLower().Trim())
                            {
                                Log.Instance.Info("md5 mismatch:\n\t" + md5.ToLower() + "\n\t" + servermd5.ToLower());
                                update = true;
                            }
                        }

                    if (update)
                        result.Add(relativePath);
                }
            }
            return result;
        }

        public bool fakeok;
        public string fakehttp;

        public bool DoHTTPRequest(string url,NLua.LuaFunction LuaCallback,bool fake,bool fakeok,string fakehttp)
        {
            if (fake) {
				NLua.Lua lastLua = Program.controller.Lua;
                Task t = Task.Run(() => {
                    Debug.Print("Preparing to fake an HTTP call to " + url);
					Thread.Sleep(1000);
					if (Program.controller.Lua is null || lastLua!=Program.controller.Lua) return;  //  in case Lua was reset while this was running
					//Debug.Print("Faking: " + (fakeok ? "OK" : "FAIL") + ", result: " + fakehttp);
					LuaCallback.Call(fakeok, fakehttp);
                });
                return true;
            } else {
                return false;
            }
        }

    }
}
