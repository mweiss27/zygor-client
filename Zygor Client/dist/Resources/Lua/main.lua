﻿
-- Warning, sender is of type ZCForm. Not a ZControl
local function saveClientPosition(sender, args)
	Log:Info("saveClientPosition")
	Prefs:Set(prefs.CLIENT_POSITION, Form:GetLocation())
end

Form:SetOnMoved(saveClientPosition)

dofile("Resources/Lua/util.lua")
dofile("Resources/Lua/topbar.lua")
dofile("Resources/Lua/accountoverview.lua")