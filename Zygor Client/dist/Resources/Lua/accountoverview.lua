﻿Log:Info("accountOverview.lua running [lua]")

local container = FormUtil:FindZControlById(Form, "container", "contentView", "homeView", "accountOverview")
if container then
	
	local accountName = FormUtil:FindZControlById(container.Control, "accountName")
	if accountName then
		accountName:SetText("Zygor Dev 6")
	end

	local userAvatar = FormUtil:FindZControlById(container.Control, "userAvatar")
	if userAvatar then
		userAvatar:SetImage("zygor_mascot", true)
	end

	local membershipValue = FormUtil:FindZControlById(container.Control, "membershipValue")
	if membershipValue then
		membershipValue:SetText("Elite")
	end

	local subscriptionValue = FormUtil:FindZControlById(container.Control, "subscriptionValue")
	if subscriptionValue then
		subscriptionValue:SetText("Active")
	end

	local editAccountLabel = FormUtil:FindZControlById(container.Control, "editAccountLabel")
	if editAccountLabel and modifyHeaderColor then
		editAccountLabel:OnMouseEnter(modifyHeaderColor(1.50))
		editAccountLabel:OnMouseLeave(modifyHeaderColor(1/1.50))
	end

	local supportLabel = FormUtil:FindZControlById(container.Control, "supportLabel")
	if supportLabel and modifyHeaderColor then
		supportLabel:OnMouseEnter(modifyHeaderColor(1.50))
		supportLabel:OnMouseLeave(modifyHeaderColor(1/1.50))
	end

	local logoutLabel = FormUtil:FindZControlById(container.Control, "logoutLabel")
	if logoutLabel and modifyHeaderColor then
		logoutLabel:OnMouseEnter(modifyHeaderColor(1.50))
		logoutLabel:OnMouseLeave(modifyHeaderColor(1/1.50))
	end
else
	Log:Error("container is nil")
end