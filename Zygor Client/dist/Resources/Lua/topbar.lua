﻿local topbar = FormUtil:FindZControlById(Form, "container", "topbar")
if topbar then

	local function closeClient(sender, args)
		Form:Shutdown()
	end

	local function minimizeClient(sender, args)
		Form:Minimize()
	end

	local function highlightBackground(sender, args)
		sender:SetBackColor("#555555")
	end

	local function unhighlightBackground(sender, args)
		sender:SetBackColor("Transparent")
	end

	function modifyHeaderColor(scale)
		return function(sender, args)
			local selected = sender:ReadTag("selected", false)
			if selected then
				return
			end
			local color = sender:GetForeColor()

			if #color == 7 then
				local r = tonumber(color:sub(2, 3), 16)
				local g = tonumber(color:sub(4, 5), 16)
				local b = tonumber(color:sub(6, 7), 16)

				h, s, l, a = rgbToHsl(r, g, b, 1)
				l = l * scale

				r, g, b = hslToRgb(h, s, l, a)
				local ret = string.format("#%02x%02x%02x", r, g, b)

				sender:SetForeColor(ret)
			end
		end
	end

	-- Triggered when the user selects a different header.
	-- zcontrol is the ZControl object for the Label that was selected
	local function onHeaderChange(zcontrol)
		
	end

	local topleft = FormUtil:FindZControlById(topbar.Control, "topleft")
	if topleft then

		local function selectHeader(sender, args)
			local headers = FormUtil:FindZControlsByType(topleft.Control, "label")
			local wasSelected = sender:ReadTag("selected", false)

			for i = 1, Util:Len(headers) do
				local header = headers[i-1]
				header:SetTag("selected", false)
				header:SetForeColor("#777777")
			end
			sender:SetTag("selected", true)
			sender:SetForeColor("#CCCCCC")

			if not wasSelected then
				onHeaderChange(sender)
			end

		end
		
		local homeLabel = FormUtil:FindZControlById(topleft.Control, "homeLabel")
		if homeLabel then
			homeLabel:OnMouseEnter(modifyHeaderColor(1.75))
			homeLabel:OnMouseLeave(modifyHeaderColor(1/1.75))
			homeLabel:OnMouseClick(selectHeader)
		end

		local newsLabel = FormUtil:FindZControlById(topleft.Control, "newsLabel")
		if newsLabel then
			newsLabel:OnMouseEnter(modifyHeaderColor(1.75))
			newsLabel:OnMouseLeave(modifyHeaderColor(1/1.75))
			newsLabel:OnMouseClick(selectHeader)
		end

		selectHeader(homeLabel)

	end

 	local topright = FormUtil:FindZControlById(topbar.Control, "topright")
	if topright then

		local closeWrapper = FormUtil:FindZControlById(topright.Control, "closeWrapper")
		if closeWrapper then
			closeWrapper:OnMouseEnter(highlightBackground)
			closeWrapper:OnMouseLeave(unhighlightBackground)
			closeWrapper:OnMouseClick(closeClient)
		end

		local minimizeWrapper = FormUtil:FindZControlById(topright.Control, "minimizeWrapper")
		if minimizeWrapper then
			minimizeWrapper:OnMouseEnter(highlightBackground)
			minimizeWrapper:OnMouseLeave(unhighlightBackground)
			minimizeWrapper:OnMouseClick(minimizeClient)
		end

	end

end