import os
import os.path
import shutil
import hashlib
import paramiko
import sys

def fmd5(f):
	md5 = hashlib.md5()
	with open(f, "rb") as file:
		for chunk in iter(lambda: file.read(4096), b""):
			md5.update(chunk)
	return md5.hexdigest()

if len(sys.argv) < 3:
	print("Usage: python gather.py <ftpusername> <relpath/to/privkey>")
	exit()

print("\n")

user = sys.argv[1]
keypath = sys.argv[2]

# Copy the files we want to publish
alldirs = [
	"Resources/Lua",
	"Resources/Views",
]

existingfiles = {}
if os.path.exists("Resources"):
	for root, subdirs, files in os.walk("Resources"):
		for f in files:
			relative = os.path.join(root, f).replace("\\", "/")
			md5 = fmd5(relative)
			existingfiles[relative] = md5

# Clean our Resources directory
if os.path.exists("./Resources"):
	shutil.rmtree("./Resources", ignore_errors=True)

if not os.path.exists("./Resources"):
	os.mkdir("./Resources")

# Copy all Resources over
# Fresh copy
for file in alldirs:
	src = "../" + file
	dest = "./" + file
	shutil.copytree(src, dest)

currentfiles = {}
if os.path.exists("Resources"):
	for root, subdirs, files in os.walk("Resources"):
		for f in files:
			relative = os.path.join(root, f).replace("\\", "/")
			md5 = fmd5(relative)
			currentfiles[relative] = md5

toupload = []
for key in currentfiles:
	if key not in existingfiles:
		toupload.append(key)
	else:
		existingmd5 = existingfiles[key]
		currentmd5 = currentfiles[key]
		if existingmd5 != currentmd5:
			toupload.append(key)

if len(toupload) == 0:
	print("No files have changed. Exiting.")
	exit()

if os.path.exists("./manifest"):
	os.remove("./manifest")

# Generate our manifest
print("Generating a manifest from " + str(currentfiles))
manifest = ""
for f in currentfiles:
	
	manifest += f + "," + fmd5(f) + "\n"

with open("./manifest", "w") as f:
	f.write(manifest)



try:
	host = "ftp.zygorguides.com"
	port = 6622
	account = user
	key = paramiko.RSAKey.from_private_key_file(keypath)

    
	transport = paramiko.Transport((host, int(port)))
	transport.connect(username=account, pkey=key)
	sftp = paramiko.SFTPClient.from_transport(transport)

	toupload.append("manifest")

	for f in toupload:
		dest = "/home/wowzygor/public_html/client/" + f
		src = "./" + f

		print("Attempting to upload " + src + " to " + dest)

		#print(dest)
		arr = dest.split('/')[1:] # Remove the first, since our dest always starts with /, making [0] an empty string
		#print(arr)
		for i in range(0, len(arr)):
			dir = "/".join(arr[0:i])
			fullPath = "/" + dir
			try: 
				sftp.mkdir(fullPath)
				print("Created " + fullPath)
			except:
				pass
		sftp.put(src, dest)

	sftp.close()

	print("Done")
except Exception as e:
	print("Error")
	import sys, traceback
	traceback.print_exc(file=sys.stdout)
	print(e)