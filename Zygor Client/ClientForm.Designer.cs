﻿using System.Drawing;
using System.Windows.Forms;
using Zygor_Client.UserControls;
using System.ComponentModel;

namespace Zygor_Client
{
    partial class ClientForm
    {

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ClientForm));
            this.clientContainer = new System.Windows.Forms.FlowLayoutPanel();
            this.topBar = new Zygor_Client.UserControls.TopBar();
            this.contentView = new Zygor_Client.UserControls.ContentView();
            this.bottomBar = new Zygor_Client.UserControls.BottomBar();
            this.trayIcon = new System.Windows.Forms.NotifyIcon(this.components);
            this.clientContainer.SuspendLayout();
            this.SuspendLayout();
            // 
            // clientContainer
            // 
            this.clientContainer.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.clientContainer.Controls.Add(this.topBar);
            this.clientContainer.Controls.Add(this.contentView);
            this.clientContainer.Controls.Add(this.bottomBar);
            this.clientContainer.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.clientContainer.Location = new System.Drawing.Point(1, 1);
            this.clientContainer.Name = "clientContainer";
            this.clientContainer.Size = new System.Drawing.Size(1102, 602);
            this.clientContainer.TabIndex = 0;
            // 
            // topBar
            // 
            this.topBar.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.topBar.Location = new System.Drawing.Point(0, 0);
            this.topBar.Margin = new System.Windows.Forms.Padding(0);
            this.topBar.Name = "topBar";
            this.topBar.Size = new System.Drawing.Size(1100, 45);
            this.topBar.TabIndex = 0;
            // 
            // contentView
            // 
            this.contentView.BackColor = System.Drawing.Color.Black;
            this.contentView.Location = new System.Drawing.Point(0, 45);
            this.contentView.Margin = new System.Windows.Forms.Padding(0);
            this.contentView.Name = "contentView";
            this.contentView.Size = new System.Drawing.Size(1100, 508);
            this.contentView.TabIndex = 5;
            // 
            // bottomBar
            // 
            this.bottomBar.BackColor = System.Drawing.Color.Black;
            this.bottomBar.Location = new System.Drawing.Point(0, 553);
            this.bottomBar.Margin = new System.Windows.Forms.Padding(0);
            this.bottomBar.Name = "bottomBar";
            this.bottomBar.Size = new System.Drawing.Size(1100, 45);
            this.bottomBar.TabIndex = 6;
            // 
            // trayIcon
            // 
            this.trayIcon.Icon = ((System.Drawing.Icon)(resources.GetObject("trayIcon.Icon")));
            this.trayIcon.Text = "Zygor Guides Client";
            this.trayIcon.Visible = true;
            this.trayIcon.Click += new System.EventHandler(this.trayIcon_Click);
            // 
            // ClientForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.ClientSize = new System.Drawing.Size(1104, 604);
            this.ControlBox = false;
            this.Controls.Add(this.clientContainer);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "ClientForm";
            this.Text = "Zygor Guides Client";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.ClientForm_FormClosing);
            this.clientContainer.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        public FlowLayoutPanel clientContainer;
        public TopBar topBar;
        public NotifyIcon trayIcon;
        public IContainer components;
        public ContentView contentView;
        public BottomBar bottomBar;
    }
}

