﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using NLua;
using Zygor_Client.CustomControls;
using Zygor_Client.Util;

namespace Zygor_Client
{
    public partial class ZCForm : Form
    {

        private Dictionary<Control, Action> _registeredEvents = new Dictionary<Control, Action>();

        public ZCForm()
        {
            InitializeComponent();

        }

        public void Shutdown()
        {
            //TODO: Perform any actions before exiting
            Application.Exit();
        }

        public void Minimize()
        {
            if (Opacity >= 1.0f)
            {
                Opacity -= 0.001f;
                new Thread(() =>
                {
                    Thread.CurrentThread.IsBackground = true;
                    
                    while (!IsDisposed && Opacity > 0f)
                        FormUtil.Instance.Invoke(this, () =>
                        {
                            if (!IsDisposed)
                                Opacity -= 0.05f;
                            Thread.Sleep(10);
                        });

                    FormUtil.Instance.Invoke(this, Hide);

                }).Start();
            }
        }

        public void Restore()
        {
            if (Opacity <= 0.0f)
            {
                Opacity += 0.001f;
                new Thread(() =>
                {
                    Thread.CurrentThread.IsBackground = true;

                    FormUtil.Instance.Invoke(this, Show);
                    while (!IsDisposed && Opacity < 1f)
                        FormUtil.Instance.Invoke(this, () =>
                        {
                            if (!IsDisposed)
                                Opacity += 0.05f;
                            Thread.Sleep(10);
                        });

                }).Start();
            }
        }

        public void Center()
        {
            CenterToScreen();
        }

        public string GetLocation()
        {
            var point = Location;
            return $"{point.X},{point.Y}";
        }

        public Control Build(FormUtil utils, string path)
        {
            var formBuilder = new FormBuilder(utils);
            var container = formBuilder.ParseXml(path);
            return container?.Control;
        }

        public void Cleanup()
        {
            ResizeEnd -= OnMoved;
            _onmove = null;
        }

        private LuaFunction _onmove;
        private void OnMoved(object sender, EventArgs args)
        {
            _onmove?.Call(this, args);
        }

        public void SetOnMoved(LuaFunction func)
        {
            ResizeEnd += OnMoved;
        }

        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            if (keyData == (Keys.Control | Keys.R))
            {
                Program.controller.ReloadUI();
                return true;
            }
            return base.ProcessCmdKey(ref msg, keyData);
        }

		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad(e);
			Program.controller.ReloadUI();
		}
	}
}
