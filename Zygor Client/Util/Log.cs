﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using log4net;
using log4net.Core;
using Zygor_Client.CustomControls;

namespace Zygor_Client.Util
{
    public class Log
    {

        private readonly ILog _log = LogManager.GetLogger("ZClient");
        
        public static Log Instance { get; private set; }

        public Log()
        {
            Instance = this;
        }

        public void Info(string message)
        {
            System.Diagnostics.Debug.WriteLine($"Info - {message}");
            _log.Info(message);
        }

        public void Error(string message)
        {
            System.Diagnostics.Debug.WriteLine($"Error - {message}");
            _log.Error(message);
        }

        public void Debug(string message)
        {
#if DEBUG
            System.Diagnostics.Debug.WriteLine($"Debug - {message}");
            _log.Debug(message);
#endif
        }

        public void Fatal(string message, bool alert=true)
        {
            message = message.Trim();
            System.Diagnostics.Debug.WriteLine($"Fatal - {message}");
            _log.Fatal(message);
            if (alert)
            {
                var messageBox = new ZMessage(message);

                messageBox.ShowDialog();
            }
        }

    }
}
