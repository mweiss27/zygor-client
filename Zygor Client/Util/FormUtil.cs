﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Drawing.Text;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Svg;
using Zygor_Client.CustomControls;

namespace Zygor_Client.Util
{
    public class FormUtil
    {

        private readonly Color ColorWindowControlHighlighted = Color.FromArgb(255, 64, 64, 64);
        private readonly Color ColorWindowControlNormal = Color.FromArgb(255, 0, 0, 0);

        public readonly string StopHighlight = "StopHighlight";
        public readonly string StartColor = "StartColor";

        public Dictionary<string, Type> RecognizedTypes { get; private set; }
        
        public FontFamily OpenSans { get; set; }

        public static FormUtil Instance { get; set; } = new FormUtil();

        public FormUtil()
        {
            OpenSans = new FontFamily("Arial Black");
            RecognizedTypes = new Dictionary<string, Type>
            {
                { "label", typeof(Label) },
                { "image", typeof(PictureBox) },
                { "panel", typeof(Panel) },
                { "flow", typeof(FlowLayoutPanel) },
                { "checkbox", typeof(ZCheckBox) },
                { "radiobutton", typeof(ZRadioButton) },
                { "button", typeof(ZButton) }
            };
        }

        public void InitFont()
        {
            var pfc = new PrivateFontCollection();
            byte[] fontByte = Properties.Resources.OpenSans_Regular;
            var fontLen = fontByte.Length;
            var data = Marshal.AllocCoTaskMem(fontLen);
            Marshal.Copy(fontByte, 0, data, fontLen);
            pfc.AddMemoryFont(data, fontLen);

            OpenSans = pfc.Families[0];
        }

        public void UpLight(object sender, EventArgs args)
        {
            var control = sender as Control;
            if (control != null)
            {
                var stopHighlight = (bool) ReadTag(control, StopHighlight, false);
                if (stopHighlight)
                    return;

                SetTag(control, StartColor, control.ForeColor);
                control.ForeColor = ControlPaint.Light(control.ForeColor, 1.1f);
            }
        }

        public void RemoveBackgroundHighlight(object sender, EventArgs args)
        {
            var control = sender as Control;
            if (control != null)
            {
                control.BackColor = ColorWindowControlNormal;
            }
        }

        public void DownLight(object sender, EventArgs args)
        {
            var control = sender as Control;
            if (control != null)
            {
                var startColor = (Color) ReadTag(control, StartColor, control.ForeColor);
                control.ForeColor = startColor;
            }
        }

        public void AddBackgroundHighlight(object sender, EventArgs args)
        {
            var control = sender as Control;
            if (control != null)
            {
                control.BackColor = ColorWindowControlHighlighted;
            }

        }

        public static object ReadTag(Control comp, string key, object def)
        {
            var tag = comp.Tag;
            if (tag == null)
                return def;

            if (!(tag is Dictionary<string, object>))
                return def;

            var dictionary = (Dictionary<string, object>) tag;
            if (dictionary.ContainsKey(key))
                return dictionary[key];

            return def;
        }

        public static void SetTag(Control comp, string key, object value)
        {
            if (!(comp.Tag is Dictionary<string, object>))
                comp.Tag = new Dictionary<string, object>();

            var dictionary = (Dictionary<string, object>) comp.Tag;
            dictionary[key] = value;
        }

        public Bitmap ResizeImage(Image image, int width, int height)
        {
            var destRect = new Rectangle(0, 0, width, height);
            var destImage = new Bitmap(width, height);

            destImage.SetResolution(image.HorizontalResolution, image.VerticalResolution);

            using (var graphics = Graphics.FromImage(destImage))
            {
                graphics.CompositingMode = CompositingMode.SourceCopy;
                graphics.CompositingQuality = CompositingQuality.HighQuality;
                graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
                graphics.SmoothingMode = SmoothingMode.HighQuality;
                graphics.PixelOffsetMode = PixelOffsetMode.HighQuality;

                using (var wrapMode = new ImageAttributes())
                {
                    wrapMode.SetWrapMode(WrapMode.TileFlipXY);
                    graphics.DrawImage(image, destRect, 0, 0, image.Width, image.Height, GraphicsUnit.Pixel, wrapMode);
                }
            }

            return destImage;
        }

        public Bitmap CropToCircle(Image srcImage, Color backColor)
        {

            Bitmap dstImage = new Bitmap(srcImage.Width, srcImage.Height, srcImage.PixelFormat);
            Graphics g = Graphics.FromImage(dstImage);
            g.SmoothingMode = SmoothingMode.HighQuality;
            var ss = 8; // Stroke Size
            
            g.DrawImage(srcImage, 0, 0);
            //g.DrawImage(ResizeImage(Properties.Resources.circleborder512, srcImage.Width, srcImage.Height), 0, 0);
            using (Pen pen = new Pen(Color.Black, ss))
                g.DrawEllipse(pen, ss, ss, srcImage.Width-ss*2, srcImage.Height-ss * 2);

            using (Brush brush = new SolidBrush(backColor))
            using (GraphicsPath path = new GraphicsPath())
            {
                var bounds = new Rectangle(0, 0, srcImage.Width, srcImage.Height);
                var inflated = Rectangle.Inflate(bounds, -ss, -ss);
                path.AddEllipse(inflated);
                path.AddRectangle(bounds);
                g.FillPath(brush, path);
            }

            return dstImage;
        }

        public Bitmap SvgToBitmap(byte[] svg, int width, int height)
        {
            using (var s = new MemoryStream(svg))
            {
                var doc = SvgDocument.Open<SvgDocument>(s);
                return doc.Draw(width, height);
            }
        }

        public Bitmap SvgToBitmap(byte[] svg, Size size)
        {
            using (var s = new MemoryStream(svg))
            {
                var doc = SvgDocument.Open<SvgDocument>(s);
                return doc.Draw(size.Width, size.Height);
            }
        }

        public void Invoke(Control c, Action a)
        {
            if (c.InvokeRequired)
                c.Invoke(new MethodInvoker(a));
            else
                a();
        }

        public IEnumerable<Control> EnumerateComponents(Control root, bool recurseContainers=true)
        {
            var list = new List<Control>();
            for (var i = 0; i < root.Controls.Count; i++)
            {
                var control = root.Controls[i];
                if (recurseContainers && control.Controls.Count > 0)
                {
                    list.AddRange(EnumerateComponents(control, true));
                    list.Add(control);
                }
                else
                    list.Add(control);
            }
            return list;
        }

        public Control GetFirstParentOfType(Control control, Type type)
        {
            Control result = control;
            while (result != null)
            {
                if (result.GetType() == type)
                    return result;

                result = result.Parent;
            }

            return null;
        }

        public ZControl[] FindZControlsByType(Control control, string type)
        {
            if (RecognizedTypes.ContainsKey(type))
            {
                var result = new List<ZControl>();
                var typeO = RecognizedTypes[type];

                foreach (Control sub in control.Controls)
                {
                    if (sub.GetType() == typeO)
                        result.Add((ZControl)ReadTag(sub, "ZControl", null));

                }

                return result.ToArray();
            }

            return null;
        }

        public ZControl FindZControlById(Control control, params string[] ids)
        {
            if (ids.Length == 0)
                return (ZControl) ReadTag(control, "ZControl", null);
            if (control == null)
                return null;

            var id = ids[0];
            foreach (Control sub in control.Controls)
                if (sub.Name == id) {
                    if (ids.Length >= 2)
                        return FindZControlById(sub, ids.Skip(1).ToArray());

                    return (ZControl) ReadTag(sub, "ZControl", null);
                }

            return null;
        }
        
    }

}
