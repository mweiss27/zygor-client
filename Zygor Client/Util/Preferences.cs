﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NLua;
using Zygor_Client.Properties;

namespace Zygor_Client.Util
{
    public class Preferences
    {

        public readonly string CLIENT_POSITION = "client_position";

        public void Set(string key, string value, bool save=true)
        {
            Settings.Default[key] = value;
            if (save)
                Settings.Default.Save();
        }

        public object Get(string key, object def = null)
        {
            return Settings.Default[key] ?? def;
        }

        public void SavePreferences(LuaTable table)
        {
            foreach (var key in table.Keys) 
            {
                var value = table[key];
                Set(key.ToString(), value.ToString(), false);
            }
            Settings.Default.Save();
        }

        public Dictionary<string ,string> ReadPreferences()
        {

            return new Dictionary<string, string> { };
        }

    }
}
