﻿using System.Drawing;

namespace Zygor_Client.Util
{
    // General utility functions
    public class GeneralUtil
    {

        public int Len(object[] array)
        {
            return array.Length;
        }
        
        public Color ColorFromHtml(string color)
        {
            return ColorTranslator.FromHtml(color);
        }
    }
}
