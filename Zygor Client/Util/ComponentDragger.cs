﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Zygor_Client.Util
{
    public class ComponentDragger : IDisposable
    {

        private Form _form;
        private Point? _pressed;

        public bool Active { get; private set; } = false;

        public ComponentDragger(Control c)
        {
            _form = c.FindForm();

            c.ParentChanged += OnParentChanged;
            c.MouseDown += OnMouseDown;
            c.MouseMove += OnMouseMove;
            c.MouseUp += OnMouseUp;
        }

        private void OnParentChanged(object sender, EventArgs args)
        {
            _pressed = null;
            Dispose();
        }

        private void OnMouseDown(object sender, MouseEventArgs args)
        {
            Debug.WriteLine("OnMouseDown!");
            _pressed = args.Location;
            Active = true;
        }

        private void OnMouseMove(object sender, MouseEventArgs e)
        {
            if (_pressed != null && _form != null)
            {
                _form.Location = new Point(
                    (_form.Location.X - _pressed.Value.X) + e.X, (_form.Location.Y - _pressed.Value.Y) + e.Y);

                _form.Update();
            }
        }

        private void OnMouseUp(object sender, MouseEventArgs args)
        {
            Debug.WriteLine("OnMouseUp!");
            _pressed = null;
            Active = false;
        }

        public void Dispose()
        {
            if (_form != null)
            {
                _form.MouseDown -= OnMouseDown;
                _form.MouseMove -= OnMouseMove;
                _form.MouseUp -= OnMouseUp;
                _form?.Dispose();
            }
            Active = false;
        }
    }
}
