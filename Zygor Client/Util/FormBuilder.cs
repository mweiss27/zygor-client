﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using System.Xml;
using Zygor_Client.CustomControls;

namespace Zygor_Client.Util
{
    public class FormBuilder
    {
        
        private FormUtil _util;

        public FormBuilder(FormUtil util)
        {
            _util = util;
        }

        public ZControl ParseXml(string file)
        {
            if (file == null)
                throw new Exception("Invalid file provided. Must not be null");

            var doc = new XmlDocument();
            try
            {
                doc.Load(file);

                var root = doc.FirstChild;
                if (root.Name == "xml")
                    root = root.NextSibling;

                return ParseNode(root);
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
                Log.Instance.Fatal("Error parsing the UI.");
                return null;
            }
        }

        private ZControl ParseNode(XmlNode node)
        {
            var type = node.Name;

            if (type == "load")
            {
                var file = node.Attributes?["file"];
                return ParseXml(file?.Value);
            }

            if (_util.RecognizedTypes.ContainsKey(type))
            {
                var clazz = _util.RecognizedTypes[type];
                var control = new ZControl(Activator.CreateInstance(clazz) as Control);
                if (control.Control == null)
                    throw new Exception("Invalid type created for " + type);

                FormUtil.SetTag(control.Control, "ZControl", control);

                SetupControl(control, node.Attributes);

                var children = node.ChildNodes;
                for (var i = 0; i < children.Count; i++)
                    control.Control.Controls.Add(ParseNode(children[i]).Control);

                return control;
            }
            else
            {
                throw new Exception("Unrecognized component type: " + type);
            }
        }

		void SetHeight(Control control) {
			var percent = (double)FormUtil.ReadTag(control, "hScale", 100.0);
			control.Height = (int)(control.Parent.Height * percent);
		}

		void SetWidth(Control control) {
			var percent = (double)FormUtil.ReadTag(control, "wScale", 100.0);
			control.Width = (int)(control.Parent.Width * percent);
		}

		private void SetupControl(ZControl zControl, XmlAttributeCollection attributes)
        {
            System.Globalization.NumberFormatInfo num_format = new System.Globalization.CultureInfo("en-US", false).NumberFormat;

            var control = zControl.Control;

            // These will get overidden below if they want to
            control.ForeColor = Color.White; 
            control.Font = new Font(_util.OpenSans, 10f);
            control.Margin = Padding.Empty;
            control.Padding = Padding.Empty;
            control.Anchor = AnchorStyles.None;
            control.Location = Point.Empty;
            control.Left = 0;


            if (control is Label)
                ((Label) control).TextAlign = ContentAlignment.MiddleLeft;
            
       
            for (var i = 0; i < attributes.Count; i++)
            {
                var attribute = attributes[i];
                var name = attribute.Name;
                if (name == "id")
                    control.Name = attribute.Value;

                else if (name == "enabled")
                    control.Enabled = bool.Parse(attribute.Value);

                else if (name == "borderstyle")
                {
                    var panel = control as Panel;
                    if (panel == null)
                        throw new Exception("Invalid borderstyle parameter. Can only be used on panel-type nodes");

                    switch (attribute.Value)
                    {
                        case "fixedsingle":
                            panel.BorderStyle = BorderStyle.FixedSingle;
                            break;
                    }
                }

                else if (name == "text")
                    control.Text = attribute.Value;

                else if (name == "fontsize")
                {
                    var tokens = attribute.Value.Split(' ');
                    float size = 11f;
                    FontStyle style = FontStyle.Regular;

                    if (tokens.Length >= 1)
                        size = float.Parse(tokens[0],num_format);
                    if (tokens.Length >= 2)
                    {
                        switch (tokens[1])
                        {
                            case "bold":
                                style = FontStyle.Bold;
                                break;
                            case "italic":
                                style = FontStyle.Italic;
                                break;

                        }

                    }

                    control.Font = new Font(control.Font.FontFamily, size, style);
                }

                else if (name == "fontstyle")
                {
                    FontStyle style = FontStyle.Regular;
                    switch (attribute.Value)
                    {
                        case "normal":
                            break;
                        case "bold":
                            style = FontStyle.Bold;
                            break;
                        case "italic":
                            style = FontStyle.Italic;
                            break;
                    }
                    control.Font = new Font(control.Font.FontFamily, control.Font.Size, style);
                }

                else if (name == "textalign")
                {
                    var label = control as Label;
                    if (label == null)
                        throw new Exception("Invalid textalign parameter. Must be applied to a <label>");

                    ContentAlignment align = ContentAlignment.BottomLeft;
                    switch (attribute.Value)
                    {
                        case "topleft":
                            align = ContentAlignment.TopLeft;
                            break;
                        case "left":
                            align = ContentAlignment.MiddleLeft;
                            break;
                        case "center":
                            align = ContentAlignment.MiddleCenter;
                            break;
                    }

                    label.TextAlign = align;
                }

                else if (name == "location")
                {
                    var tokens = attribute.Value.Split(' ');
                    if (tokens.Length != 2)
                        throw new Exception("Invalid location parameter. Must contain 2 values [x y].");

                    control.Location = new Point(int.Parse(tokens[0]), int.Parse(tokens[1]));
                }

                else if (name == "autosizemode")
                {
                    var flow = control as FlowLayoutPanel;
                    if (flow == null)
                        throw new Exception("The autosizemode attribute can only be used with <flow> elements");
                    switch (attribute.Value)
                    {
                        case "grow":
                            flow.AutoSizeMode = AutoSizeMode.GrowOnly;
                            break;
                        case "growandshrink":
                            flow.AutoSizeMode = AutoSizeMode.GrowAndShrink;
                            break;
                    }
                }

                else if (name == "autosize")
                    control.AutoSize = attribute.Value.ToLower() == "true";

                else if (name == "size")
                {
                    var tokens = attribute.Value.Split(' ');
                    if (tokens.Length != 2)
                        throw new Exception("Invalid size parameter. 2 values must be provided. Use -1 to ignore.");
                    
                    var width = tokens[0];
                    if (width.ToLower() == "h")
                        control.SizeChanged += (sender, args) => control.Width = control.Height;

                    else if (width.EndsWith("%"))
                    {
                        FormUtil.SetTag(control, "wScale", float.Parse(width.Substring(0, width.Length - 1), num_format) / 100.0);
                        control.ParentChanged += (sender, args) =>
                        {
                            SetWidth(control);
                            control.Parent.SizeChanged += (o, eventArgs) => SetWidth(o as Control);
                        };
                    }
                        
                    else
                        if (int.Parse(width) > 0)
                            control.Width = int.Parse(width);


                    var height = tokens[1];
                    if (height.ToLower() == "w")
                        control.SizeChanged += (sender, args) => control.Height = control.Width;

                    else if (height.EndsWith("%"))
                    {
                        var hf = float.Parse(height.Substring(0, height.Length - 1),num_format);
                        FormUtil.SetTag(control, "hScale", hf / 100.0);
                        control.ParentChanged += (sender, args) =>
                        {
                            SetHeight(control);
                            control.Parent.SizeChanged += (o, eventArgs) => SetHeight(o as Control);
                        };
                    }
                    else
                        if (int.Parse(height) > 0)
                            control.Height = int.Parse(height);
                }

                else if (name == "margin")
                {
                    var tokens = attribute.Value.Split(' ');
                    int top, left, bottom, right;
                    if (tokens.Length == 1)
                        left = top = right = bottom = int.Parse(tokens[0]);

                    else if (tokens.Length == 2)
                    {
                        left = right = int.Parse(tokens[0]);
                        top = bottom = int.Parse(tokens[1]);
                    }

                    else if (tokens.Length == 4)
                    {
                        left = int.Parse(tokens[0]);
                        top = int.Parse(tokens[1]);
                        right = int.Parse(tokens[2]);
                        bottom = int.Parse(tokens[3]);
                    }
                    else
                    {
                        Debug.WriteLine("[ERROR] Invalid margin. Must be 1, 2, or 4 values. Top Left Bottom Right");
                        continue;
                    }

                    control.Margin = new Padding(left, top, right, bottom);
                }


                else if (name == "anchor")
                {
                    if (attribute.Value.Contains("top"))
                        control.Anchor |= AnchorStyles.Top;

                    if (attribute.Value.Contains("left"))
                        control.Anchor |= AnchorStyles.Left;

                    if (attribute.Value.Contains("bottom"))
                        control.Anchor |= AnchorStyles.Bottom;

                    if (attribute.Value.Contains("right"))
                        control.Anchor |= AnchorStyles.Right;

                    if (attribute.Value.Contains("none"))
                        control.Anchor = AnchorStyles.None;
                }

                else if (name == "dock")
                    switch (attribute.Value)
                    {
                        case "fill":
                            control.Dock = DockStyle.Fill;
                            break;
                        case "left":
                            control.Dock = DockStyle.Left;
                            break;
                        case "top":
                            control.Dock = DockStyle.Top;
                            break;
                        case "bottom":
                            control.Dock = DockStyle.Bottom;
                            break;
                        case "right":
                            control.Dock = DockStyle.Right;
                            break;
                    }

                else if (name == "forecolor")
                    control.ForeColor = ColorTranslator.FromHtml(attribute.Value);

                else if (name == "backcolor")
                    control.BackColor = ColorTranslator.FromHtml(attribute.Value);

                else if (name == "cursor")
                    switch (attribute.Value)
                    {
                        case "default":
                            control.Cursor = Cursors.Default;
                            break;
                        case "hand":
                            control.Cursor = Cursors.Hand;
                            break;
                    }

                else if (name == "dir")
                {
                    var flow = (control as FlowLayoutPanel);
                    if (flow == null)
                        throw new Exception("The dir attribute can only be used with <flow> elements");

                    switch (attribute.Value)
                    {
                        case "td":
                            flow.FlowDirection = FlowDirection.TopDown;
                            break;
                        case "bu":
                            flow.FlowDirection = FlowDirection.BottomUp;
                            break;
                        case "ltr":
                            flow.FlowDirection = FlowDirection.LeftToRight;
                            break;
                        case "rtl":
                            flow.FlowDirection = FlowDirection.RightToLeft;
                            break;
                    }
                }

                else if (name == "image")
                {
                    Control img = (control as PictureBox);
                    if (img == null) { 
                        img = control as ZButton;
                        if (img == null)
                            throw new Exception("Invalid attribute. image can only be used for PictureBox");
                    }

                    bool b;
                    var isCropped = Boolean.TryParse(attributes.GetNamedItem("cropped")?.ToString(), out b);

                    var resource = Properties.Resources.ResourceManager.GetObject(attribute.Value);

                    if (resource is byte[])
                    {
                        zControl.SetImage(resource as byte[]);
                        img.SizeChanged += (sender, args) => zControl.SetImage(resource as byte[]);
                    }
                    else
                        zControl.SetImage(resource as Bitmap, null, isCropped);

                }
                else if (name == "sizemode")
                {
                    var img = control as PictureBox;
                    if (img == null)
                        throw new Exception("Invalid attribute. sizemode can only be used for PictureBox");

                    switch (attribute.Value)
                    {
                        case "zoom":
                            img.SizeMode = PictureBoxSizeMode.Zoom;
                            break;
                        case "autosize":
                            img.SizeMode = PictureBoxSizeMode.AutoSize;
                            break;
                        case "centerimage":
                            img.SizeMode = PictureBoxSizeMode.CenterImage;
                            break;
                        case "normal":
                            img.SizeMode = PictureBoxSizeMode.Normal;
                            break;
                        case "stretchimage":
                            img.SizeMode = PictureBoxSizeMode.StretchImage;
                            break;
                    }
                }
            }
            
        }

    }
}
