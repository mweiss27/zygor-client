﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zygor_Client.Events
{
    public class ScrollbarEventArgs : EventArgs
    {

        public int OffsetY { get; set; }

    }
}
