﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Windows.Forms;
using Zygor_Client.Controllers;

namespace Zygor_Client
{
    static class Program
    {
        public static Dictionary<string, bool> argflags = new Dictionary<string, bool>();

        public static Controller controller;

        [STAThread]
        static void Main(string[] args)
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(true);

            
            foreach (string s in args)
            {
                if (s=="/disableupdates")
                {
                    argflags["disableupdates"] = true;
                }
            }
            
            controller = new Controller();
            controller.CheckForUpdates();

            controller.FormUtil.InitFont();
            controller.Start();

            Application.Run(controller.Form);
            
        }

    }
}
