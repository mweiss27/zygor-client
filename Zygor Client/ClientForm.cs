﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Windows.Forms;
using Zygor_Client.UserControls;
using Zygor_Client.Util;

namespace Zygor_Client
{
    public partial class ClientForm : Form
    {

        #region Statics
        private static readonly Color ColorHeaderSelected = Color.FromArgb(255, 200, 200, 200);
        private static readonly Color ColorHeaderUnselected = Color.FromArgb(255, 64, 64, 64);
        public static readonly string StopHighlight = "StopHighlight";
        public static readonly string StartColor = "StartColor";

        private static ClientForm instance;
        #endregion
        
        private readonly Control[] headers;

        public HomeView HomeView { get; set; }
        public SettingsView SettingsView { get; set; }

        public ClientForm()
        {
            InitializeComponent();
            this.headers = new Control[] {this.topBar.guidesLabel, this.topBar.newsLabel };
            this.HomeView = new HomeView();
            this.SettingsView = new SettingsView();

            this.contentView.Controls.Clear();
            this.contentView.Controls.Add(this.HomeView);

            this.SetAvatarImage(Properties.Resources.zygor_mascot);

            // Setup hover listeners for all labels to give visual feedback
            this.AddHoverListeners();

            // Setup listeners for [ Guides, News ] labels
            this.AddHeaderListeners();

            this.FixFonts();

            this.InitSystemTrayIcon();
            this.FormClosed += this.OnClose;

            instance = this;
        }

        public static ClientForm Instance()
        {
            return instance;
        }

        /**
         * Adds a highlight hover event to all Labels in the form.
         * 
         */
        private void AddHoverListeners()
        {
            foreach (var component in FormUtil.EnumerateComponents(this).OfType<Label>())
            {
                component.MouseEnter += FormUtil.UpLight;
                component.MouseLeave += FormUtil.DownLight;
            }

            this.topBar.closeButtonContainer.MouseEnter += FormUtil.AddBackgroundHighlight;
            this.topBar.minimizeButtonContainer.MouseEnter += FormUtil.AddBackgroundHighlight;

            this.topBar.closeButtonContainer.MouseLeave += Util.FormUtil.RemoveBackgroundHighlight;
            this.topBar.minimizeButtonContainer.MouseLeave += Util.FormUtil.RemoveBackgroundHighlight;

        }

        private void AddHeaderListeners()
        {
            foreach (var header in this.headers)
                header.MouseClick += this.ClickHeader;

            FormUtil.SetTag(this.topBar.guidesLabel, StopHighlight, true);
        }

        private void FixFonts()
        {
            var components = FormUtil.EnumerateComponents(this.topBar, true)
                .Concat(FormUtil.EnumerateComponents(this.HomeView, true))
                .Concat(FormUtil.EnumerateComponents(this.SettingsView, true))
                .Concat(FormUtil.EnumerateComponents(this.bottomBar, true));
            foreach (var control in components)
            {
                var size = control.Font.Size;
                var style = control.Font.Style;
                Debug.WriteLine("Setting font of " + control);
                control.Font = new Font("Segoe Script", size, style);
            }
        }

        private void ClickHeader(object sender, EventArgs args)
        {
            var control = sender as Control;
            if (control != null)
            {
                var isSelected = (bool) FormUtil.ReadTag(control, StopHighlight, false);
                if (!isSelected)
                {
                    foreach (var header in this.headers)
                    {
                        FormUtil.SetTag(header, StopHighlight, false);
                        header.ForeColor = ColorHeaderUnselected;
                    }
                    FormUtil.SetTag(control, StopHighlight, true);
                    FormUtil.SetTag(control, StartColor, ColorHeaderSelected);
                    control.ForeColor = ColorHeaderSelected;
                }
            }
        }

        public void ToSettings()
        {
            try
            {
                this.SuspendLayout();
                this.contentView.Controls.Remove(this.HomeView);
                this.contentView.Controls.Add(this.SettingsView);
            }
            finally
            {
                this.ResumeLayout();
            }
        }

        public void SetAvatarImage(Image uncropped)
        {
            var ratio = uncropped.Height / uncropped.Width;
            if (uncropped.Width > 300)
                uncropped = ResizeImage(uncropped, 300, 300 * ratio);

            this.HomeView.accountOverview.avatarImage.Image = FormUtil.CropToCircle(uncropped);
        }

        
        
        private void InitSystemTrayIcon()
        {
            var menu = new ContextMenu();
            
            var exitItem = new MenuItem("Exit");
            exitItem.Click += Program.Shutdown;

            var openItem = new MenuItem("Open");
            openItem.Click += Open;

            menu.MenuItems.Add(openItem);
            menu.MenuItems.Add(exitItem);

            trayIcon.Tag = this;
            trayIcon.ContextMenu = menu;
        }

        public new void Hide()
        {
            if (Opacity >= 1.0f)
            {
                Opacity -= 0.001f;
                new Thread(() =>
                {
                    Thread.CurrentThread.IsBackground = true;


                    while (!IsDisposed && Opacity > 0f)
                        Invoke(new MethodInvoker(() =>
                        {
                            if (!IsDisposed)
                                this.Opacity -= 0.05f;
                            Thread.Sleep(10);
                        }));

                    Invoke(new MethodInvoker(base.Hide));

                }).Start();
            }
        }

        public new void Show()
        {
            if (Opacity <= 0.0f)
            {
                Opacity += 0.001f;
                new Thread(() =>
                {
                    Thread.CurrentThread.IsBackground = true;

                    Invoke(new MethodInvoker(base.Show));
                    while (!IsDisposed && Opacity < 1f)
                        Invoke(new MethodInvoker(() =>
                        {
                            if (!IsDisposed)
                                Opacity += 0.05f;
                            Thread.Sleep(10);
                        }));

                }).Start();
            }
        }

        public void Open(object sender, EventArgs e)
        {
            ClientForm form = Instance();
            
            if (form != null)
                this.Show();
        }

        private void OnClose(object sender, FormClosedEventArgs formClosedEventArgs)
        {
            this.trayIcon.Visible = false;
        }

        private void ClientForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing)
            {
                e.Cancel = true;
                this.Hide();
            }
        }

        private void trayIcon_Click(object sender, EventArgs e)
        {
            this.Open(sender, e);
        }
    }
}