﻿using System;
using System.Drawing;
using System.Windows.Forms;
using Zygor_Client.Util;

namespace Zygor_Client.CustomControls
{
    public partial class ZButton : Button
    {

        private static readonly Color ColorButtonNormal = ColorTranslator.FromHtml("#373737");
        private static readonly Color ColorButtonHovered = ColorTranslator.FromHtml("#474747");

        private static readonly Pen BorderPen = new Pen(Color.LightGray);
        private static readonly Pen WhitePen = new Pen(Color.White);
        private static readonly Brush WhiteBrush = new SolidBrush(Color.White);
        
        public ZButton()
        {
            InitializeComponent();
            Size = new Size(125, 35);
            BackColor = ColorButtonNormal;

            SetStyle(
                ControlStyles.UserPaint |
                ControlStyles.DoubleBuffer, true);

            this.MouseEnter += delegate(object sender, EventArgs args)
            {
                FormUtil.SetTag(this, "hovered", true);
                Refresh();
            };

            MouseLeave += delegate(object sender, EventArgs args)
            {
                FormUtil.SetTag(this, "hovered", false);
                Refresh();
            };
        }

        protected override void OnPaint(PaintEventArgs pe)
        {
            var graphics = pe.Graphics;
            var hovered = (bool)FormUtil.ReadTag(this, "hovered", false);

            graphics.Clear(BackColor);

            graphics.DrawRectangle(hovered ? WhitePen :  BorderPen, 0, 0, Width-1, Height-1);

            SizeF stringSize = graphics.MeasureString(Text, Font);
            var cx = Width / 2f - stringSize.Width / 2f;
            var cy = Height / 2f - stringSize.Height / 2f;

            if (Image != null)
            {
                var iw = Image.Width;
                var ih = Image.Height;

                var ciy = Height / 2f - ih / 2f;

                cx = (Width / 2f) - (stringSize.Width + iw) / 2f;
                graphics.DrawImage(Image, cx-3, ciy);
                graphics.DrawString(Text, Font, WhiteBrush, cx + iw+ 3, cy);
            }
            else
                graphics.DrawString(Text, Font, WhiteBrush, cx, cy);


        }

    }
}
