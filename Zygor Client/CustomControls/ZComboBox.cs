﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Zygor_Client.Util;

namespace Zygor_Client.CustomControls
{
    public partial class ZComboBox : ComboBox
    {

        private static readonly Color BoxBackgroundColor = Color.FromArgb(255, 32, 32, 32);
        private static readonly Color BoxBackgroundColorHovered = Color.FromArgb(255, 48, 48, 48);

        private readonly Brush BackgroundHovered = new SolidBrush(BoxBackgroundColorHovered);
        private readonly Brush BackgroundNormal = new SolidBrush(BoxBackgroundColor);
        private readonly Brush WhiteBrush = new SolidBrush(Color.White);

        public ZComboBox()
        {
            InitializeComponent();
            DropDownStyle = ComboBoxStyle.DropDownList;
            FlatStyle = FlatStyle.Popup;
            
            DrawMode = DrawMode.OwnerDrawFixed;
            ItemHeight = 18;
            SetStyle(ControlStyles.UserPaint | ControlStyles.DoubleBuffer, true);

        }

        protected override void OnPaint(PaintEventArgs pe)
        {
            var graphics = pe.Graphics;
            using (Brush brush = new SolidBrush(BoxBackgroundColor))
                graphics.FillRectangle(brush, 0, 0, Width, Height);

            var arrowSize = Height-2;

            var img = FormUtil.Instance.SvgToBitmap(Properties.Resources.dropdownarrow128, arrowSize, arrowSize);
            graphics.DrawImage(img, Width-arrowSize-1, 1);

            using (Pen pen = new Pen(Color.LightGray))
                graphics.DrawRectangle(pen, 0, 0, Width-1, Height-1);

            graphics.DrawString(this.SelectedItem.ToString(), Font, WhiteBrush, 3, 3);

            base.OnPaint(pe);
        }


        private void combobox_DrawItem(object sender, DrawItemEventArgs e)
        {

            if ((e.State & DrawItemState.Selected) == DrawItemState.Selected)
                e.Graphics.FillRectangle(BackgroundHovered, e.Bounds);

            else
                e.Graphics.FillRectangle(BackgroundNormal, e.Bounds);

            e.Graphics.DrawString(this.Items[e.Index].ToString(), e.Font, WhiteBrush, e.Bounds);
        }
    }
}
