﻿using NLua;
using System;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Text;
using System.Windows.Forms;
using Svg;
using Zygor_Client.Util;

namespace Zygor_Client.CustomControls
{
    public class ZControl
    {

        public Control Control { get; private set; }

        public ZControl(Control control)
        {
            Control = control;
            control.MouseClick += (sender, args) =>
            {
                Debug.WriteLine($"Margin: {control.Margin}");
                Debug.WriteLine($"Padding: {control.Padding}");
                Debug.WriteLine($"Location: {control.Location}");
                ;
            };
        }

        public void SetTag(string key, object value)
        {
            FormUtil.SetTag(this.Control, key, value);
        }

        public object ReadTag(string key, object def)
        {
            return FormUtil.ReadTag(this.Control, key, def);
        }

        public void SetBackColor(string color)
        {
            SetBackColor(ColorTranslator.FromHtml(color));
        }

        public void SetBackColor(Color color)
        {
            FormUtil.Instance.Invoke(Control, () => Control.BackColor = color);
        }

        public string GetBackColor()
        {
            var c = Control.BackColor;
            return "#" + c.R.ToString("X2") + c.G.ToString("X2") + c.B.ToString("X2");
        }

        public void SetForeColor(string color)
        {
            SetForeColor(ColorTranslator.FromHtml(color));
        }

        public void SetForeColor(Color color)
        {
            FormUtil.Instance.Invoke(Control, () => Control.ForeColor = color);
        }

        public string GetForeColor()
        {
            var c = Control.ForeColor;
            return "#" + c.R.ToString("X2") + c.G.ToString("X2") + c.B.ToString("X2");
        }

        public void SetText(string text)
        {
            FormUtil.Instance.Invoke(Control, () => Control.Text = text);
        }

        public string GetText()
        {
            return Control.Text;
        }

        public void SetImage(string resource)
        {
            SetImage(Properties.Resources.ResourceManager.GetObject(resource) as Bitmap, null, false);
        }

        public void SetImage(string resource, bool crop)
        {
            SetImage(Properties.Resources.ResourceManager.GetObject(resource) as Bitmap, null, crop);
        }

        public void SetImage(Bitmap image, Size? size=null, bool? crop=false)
        {
            var picture = Control as PictureBox;
            if (picture != null)
            {
                if (size != null)
                    image = FormUtil.Instance.ResizeImage(image, size.Value.Width, size.Value.Height);

                if (crop ?? false)
                    image = FormUtil.Instance.CropToCircle(image, Control.BackColor);

                picture.Image = image;
            }

            var button = Control as ZButton;
            if (button != null)
            {
                if (size != null)
                    image = FormUtil.Instance.ResizeImage(image, size.Value.Width, size.Value.Height);

                if (crop ?? false)
                    image = FormUtil.Instance.CropToCircle(image, Control.BackColor);

                button.Image = image;
            }

        }

        public void SetImage(byte[] svg, Size? size=null)
        {
            var picture = Control as PictureBox;
            if (picture != null)
                picture.Image = FormUtil.Instance.SvgToBitmap(svg, Control.Size);

            var button = Control as ZButton;
            if (button != null)
                button.Image = FormUtil.Instance.SvgToBitmap(svg, size ?? new Size(Control.Height/2, Control.Height/2));
        }

        public void OnMouseEnter(LuaFunction func)
        {
            Control.MouseEnter += delegate(object sender, EventArgs args)
            {
                var zControl = FormUtil.ReadTag((sender as Control), "ZControl", null) as ZControl;
                if (zControl != null)
                    func.Call(zControl, args);
            };
        }

        public void OnMouseLeave(LuaFunction func)
        {
            Control.MouseLeave += delegate (object sender, EventArgs args)
            {
                var zControl = FormUtil.ReadTag((sender as Control), "ZControl", null) as ZControl;
                if (zControl != null)
                    func.Call(zControl, args);
            };
        }

        public void OnMouseDown(LuaFunction func)
        {
            Control.MouseDown += delegate(object sender, MouseEventArgs args)
            {
                var zControl = FormUtil.ReadTag((sender as Control), "ZControl", null) as ZControl;
                if (zControl != null)
                    func.Call(zControl, args);
            };
        }

        public void OnMouseClick(LuaFunction func)
        {
            Control.MouseClick += delegate (object sender, MouseEventArgs args)
            {
                var zControl = FormUtil.ReadTag((sender as Control), "ZControl", null) as ZControl;
                if (zControl != null)
                    func.Call(zControl, args);
            };
        }

    }
}
