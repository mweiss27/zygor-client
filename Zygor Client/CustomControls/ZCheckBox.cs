﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Zygor_Client.CustomControls
{
    public partial class ZCheckBox : CheckBox
    {
        private readonly Brush WhiteBrush = new SolidBrush(Color.White);
        private readonly Brush FilledBrush = new SolidBrush(Color.OrangeRed);
        private readonly Pen CirclePen = new Pen(Color.Gray);

        public ZCheckBox()
        {
            InitializeComponent();

            SetStyle(
                ControlStyles.UserPaint |
                ControlStyles.DoubleBuffer, true);
        }

        protected override void OnPaint(PaintEventArgs pe)
        {
            var graphics = pe.Graphics;
            graphics.Clear(BackColor);
            graphics.SmoothingMode = SmoothingMode.AntiAlias;

            var rectSize = 12;

            graphics.DrawRectangle(CirclePen, 0, 3, rectSize, rectSize);
            if (Checked)
                graphics.FillRectangle(FilledBrush, 3, 6, rectSize - 6, rectSize - 6);

            graphics.DrawString(Text, Font, WhiteBrush, rectSize + 3, 0);

        }
    }
}
