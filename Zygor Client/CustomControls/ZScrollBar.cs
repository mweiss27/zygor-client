﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Zygor_Client.Util;

namespace Zygor_Client.CustomControls
{
    public partial class ZScrollBar : Panel
    {
        #region Statics

        private static readonly Color TrackColor = ColorTranslator.FromHtml("#424242");
        private static readonly Brush BrushColorNormal = new SolidBrush(ColorTranslator.FromHtml("#6E6E6E"));
        private static readonly Brush BrushColorHovered = new SolidBrush(ColorTranslator.FromHtml("#8E8E8E"));

        #endregion

        private int? _pressedY = null;
        private bool _hovered = false;
        public int ThumbY { get; set; } = 0;
        public int ThumbHeight { get; set; }

        public event EventHandler<ScrollEventArgs> ScrollBarMoved;

        public ZScrollBar() : this(null)
        { }

        public ZScrollBar(IContainer container)
        {
            container?.Add(this);

            InitializeComponent();

            Dock = DockStyle.Right;
            Width = 20;
            
            MouseEnter += delegate { _hovered = true; Refresh(); };
            MouseLeave += delegate { _hovered = false; Refresh(); };
            MouseDown += delegate(object sender, MouseEventArgs args) { if (IsInThumb(args.Location)) _pressedY = args.Y; };
            MouseUp += delegate { _pressedY = null; };
            MouseMove += OnMouseMove;

            SetStyle(ControlStyles.UserMouse | ControlStyles.DoubleBuffer | ControlStyles.AllPaintingInWmPaint, true);
        }

        private void OnMouseMove(object sender, MouseEventArgs e)
        {
            if (_pressedY != null)
            {
                var dy = e.Y - _pressedY.Value;
                if (dy != 0)
                {
                    ThumbY += dy;
                    ThumbY = Math.Max(0, ThumbY);
                    ThumbY = Math.Min(Height - ThumbHeight, ThumbY);
                    ScrollBarMoved?.Invoke(this, new ScrollEventArgs(ScrollEventType.EndScroll, ThumbY));
                    FormUtil.Instance.Invoke(this, Refresh);
                }

                _pressedY = e.Y;
            }
        }

        private bool IsInThumb(Point loc)
        {
            return loc.Y >= ThumbY && loc.Y <= ThumbY + ThumbHeight;
        }
        
        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);

            var graphics = e.Graphics;
            graphics.Clear(TrackColor);

            graphics.FillRectangle(_hovered ? BrushColorHovered : BrushColorNormal, 0, ThumbY, Width, ThumbHeight);

        }
    }
}
