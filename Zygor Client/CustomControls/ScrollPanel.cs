﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Zygor_Client.CustomControls
{
    public partial class ScrollPanel : Panel
    {
        
        public ScrollPanel() : this(null)
        {
            
        }

        public ScrollPanel(IContainer container=null)
        {
            container?.Add(this);

            InitializeComponent();
            
            Margin = Padding.Empty;

            Viewport = new Panel();
            Viewport.AutoSize = false;
            Viewport.Margin = Padding.Empty;

            ScrollBar = new ZScrollBar();
            ScrollBar.Dock = DockStyle.Right;
            ScrollBar.Margin = Padding.Empty;

            Controls.Add(Viewport);
            Controls.Add(ScrollBar);

            Viewport.Location = new Point(0, 0);
            Viewport.Size = new Size(Width - ScrollBar.Width, Height);

            Viewport.SizeChanged += ViewPortOnSizeChanged;
            ScrollBar.ScrollBarMoved += ScrollBarOnScrollBarMoved;

            ScrollBar.Size = new Size(ScrollBar.Width, Height);
            Viewport.Size = new Size(Width - ScrollBar.Width, Height);
        }

        private void ViewPortOnSizeChanged(object sender, EventArgs eventArgs)
        {   
            Debug.WriteLine($"Viewport Size changed! Height {Viewport.Height}");
            var viewportHeight = Viewport.Height;
            var scrollbarHeight = ScrollBar.Height;

            var dy = viewportHeight - scrollbarHeight;
            Debug.WriteLine($"dy: {dy}");
            ScrollBar.Visible = dy > 0;
            if (ScrollBar.Visible)
            {
                var ratio = (float) ScrollBar.Height / dy;
                ScrollBar.ThumbHeight = Math.Max(5, (int) (ScrollBar.Height * ratio));
                Debug.WriteLine($"ThumbHeight: {ScrollBar.ThumbHeight}");
                ScrollBar.Refresh();
            }
        }

        private void ScrollBarOnScrollBarMoved(object sender, ScrollEventArgs scrollEventArgs)
        {
            SetViewportOffset();
        }

        // We need to shift our viewport up the percentage our scroll thumb is moved down * the difference in height between
        // the viewport and the scrollbar
        private void SetViewportOffset()
        {
            var dy = Viewport.Height - ScrollBar.Height;
            if (dy > 0)
            {
                var thumbY = ScrollBar.ThumbY;
                var maxThumbY = ScrollBar.Height - ScrollBar.ThumbHeight;

                var percentIn = (float)thumbY / maxThumbY;
                var shift = (int) Math.Ceiling(percentIn * dy);

                Viewport.Location = new Point(0, -shift);
            }
            else
                Viewport.Location = new Point(0, 0);

        }
    }
}
