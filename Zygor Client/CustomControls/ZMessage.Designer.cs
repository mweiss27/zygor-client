﻿namespace Zygor_Client.CustomControls
{
    partial class ZMessage
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.logo = new System.Windows.Forms.PictureBox();
            this.zButton = new Zygor_Client.CustomControls.ZButton();
            this.scrollPanel = new Zygor_Client.CustomControls.ScrollPanel(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.logo)).BeginInit();
            this.SuspendLayout();
            // 
            // logo
            // 
            this.logo.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.logo.Enabled = false;
            this.logo.Image = global::Zygor_Client.Properties.Resources.logo;
            this.logo.Location = new System.Drawing.Point(149, 12);
            this.logo.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.logo.Name = "logo";
            this.logo.Size = new System.Drawing.Size(200, 65);
            this.logo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.logo.TabIndex = 0;
            this.logo.TabStop = false;
            // 
            // zButton
            // 
            this.zButton.Font = new System.Drawing.Font("Arial", 10F);
            this.zButton.Location = new System.Drawing.Point(12, 303);
            this.zButton.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.zButton.Name = "zButton";
            this.zButton.Size = new System.Drawing.Size(125, 34);
            this.zButton.TabIndex = 1;
            this.zButton.Text = "Okay";
            this.zButton.UseVisualStyleBackColor = true;
            this.zButton.Click += new System.EventHandler(this.zButton1_Click);
            // 
            // scrollPanel
            // 
            this.scrollPanel.Location = new System.Drawing.Point(47, 80);
            this.scrollPanel.Margin = new System.Windows.Forms.Padding(0);
            this.scrollPanel.Name = "scrollPanel";
            this.scrollPanel.Size = new System.Drawing.Size(433, 199);
            this.scrollPanel.TabIndex = 2;
            // 
            // ZMessage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.ClientSize = new System.Drawing.Size(500, 350);
            this.Controls.Add(this.scrollPanel);
            this.Controls.Add(this.zButton);
            this.Controls.Add(this.logo);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "ZMessage";
            this.Text = "ZMessage";
            ((System.ComponentModel.ISupportInitialize)(this.logo)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox logo;
        private ZButton zButton;
        private ScrollPanel scrollPanel;
    }
}