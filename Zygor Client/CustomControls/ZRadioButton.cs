﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Zygor_Client.CustomControls
{
    public sealed partial class ZRadioButton : RadioButton
    {

        private readonly Brush WhiteBrush = new SolidBrush(Color.White);
        private readonly Brush FilledBrush = new SolidBrush(Color.OrangeRed);
        private readonly Pen CirclePen = new Pen(Color.Gray);

        public ZRadioButton()
        {
            InitializeComponent();

            SetStyle(
                ControlStyles.UserPaint |
                ControlStyles.DoubleBuffer, true);
        }

        protected override void OnPaint(PaintEventArgs pe)
        {
            var graphics = pe.Graphics;
            graphics.Clear(BackColor);
            graphics.SmoothingMode = SmoothingMode.AntiAlias;

            var circleSize = 12;

            graphics.DrawEllipse(CirclePen, 0, 3, circleSize, circleSize);
            if (Checked)
                graphics.FillEllipse(FilledBrush, 3, 6, circleSize - 6, circleSize - 6);

            graphics.DrawString(Text, Font, WhiteBrush, circleSize + 3, 0);
            
        }
    }
}
