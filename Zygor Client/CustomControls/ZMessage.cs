﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Zygor_Client.Extensions;
using Zygor_Client.Util;

namespace Zygor_Client.CustomControls
{
    public partial class ZMessage : Form
    {

        public TextBox TextBox { get; }

        public ZMessage(string message="")
        {
            InitializeComponent();

            scrollPanel.Viewport.Size = new Size(scrollPanel.Width - scrollPanel.ScrollBar.Width, scrollPanel.Height);

            TextBox = new TextBox
            {
                BackColor = Color.Black,
                ForeColor = Color.White,
                Multiline = true,
                Width = scrollPanel.Viewport.Width,
                BorderStyle = BorderStyle.None,
                ReadOnly = true,
                AutoSize = true
            };
            TextBox.KeyDown += delegate(object sender, KeyEventArgs args)
            {
                if (args.Control && args.KeyCode == Keys.A)
                    TextBox.SelectAll();
            };

            scrollPanel.Viewport.Controls.Add(TextBox);

            new ComponentDragger(this);

            Shown += (sender, args) =>
                SetText(message);
        }

        public void SetText(string message)
        {
            var size = TextRenderer.MeasureText(message, TextBox.Font, scrollPanel.Viewport.Size, TextFormatFlags.WordBreak);
            TextBox.Text = message;
            TextBox.Height = size.Height;
            scrollPanel.Viewport.Height = TextBox.Height;
        }
        
        private void zButton1_Click(object sender, EventArgs e)
        {
            Dispose();
        }
        
    }
}
