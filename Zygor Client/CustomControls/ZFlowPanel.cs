﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Zygor_Client.CustomControls
{
    public partial class ZFlowPanel : FlowLayoutPanel
    {

        public bool Center { get; set; } = false;

        public ZFlowPanel()
        {
            InitializeComponent();

            ControlAdded += OnControlAdded;
            ControlRemoved += OnControlRemoved;
            SizeChanged += OnSizeChanged;
        }

        private void OnSizeChanged(object sender, EventArgs eventArgs)
        {
            if (Center)
                foreach (Control control in Controls)
                    CenterChild(control);

        }

        private void OnControlAdded(object sender, ControlEventArgs args)
        {
            args.Control.SizeChanged += ControlOnSizeChanged;
        }

        private void OnControlRemoved(object sender, ControlEventArgs args)
        {
            args.Control.SizeChanged -= ControlOnSizeChanged;
        }

        private void ControlOnSizeChanged(object sender, EventArgs args)
        {
            if (Center) { 
                var control = sender as Control;
                if (control != null)
                    CenterChild(control);
            }
        }

        private void CenterChild(Control child)
        {
            if (FlowDirection == FlowDirection.TopDown || FlowDirection == FlowDirection.BottomUp)
                CenterHorizontally(child);
            else
                CenterVertically(child);
        }

        private void CenterHorizontally(Control child)
        {
            var ml = (Width - child.Width) / 2.0;
            var mt = child.Margin.Top;
            var mr = child.Margin.Right;
            var mb = child.Margin.Bottom;
            
            //child.Margin = new Padding(0, mt, mr, mb);
        }

        private void CenterVertically(Control child)
        {
            var ml = child.Margin.Left;
            var mt = (Height - child.Height) / 2.0;
            var mr = child.Margin.Right;
            var mb = child.Margin.Bottom;

            //child.Margin = new Padding(ml, 0, mr, mb);
        }

    }
}
