﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Zygor_Client.Util;

namespace Zygor_Client.CustomControls
{
    public partial class SettingsTabLabel : Label
    {

        private readonly Pen OrangePen = new Pen(Color.OrangeRed, 2);
        private readonly Brush WhiteBrush = new SolidBrush(Color.White);
        private readonly Font font = new Font("Open Sans", 11f);

        private static readonly Color SettingsTabBackgroundHovered = Color.FromArgb(255, 55, 55, 55);
        private static readonly Color SettingsTabBackgroundSelected = Color.FromArgb(255, 80, 80, 80);

        public SettingsTabLabel()
        {
            InitializeComponent();
        }

        protected override void OnPaint(PaintEventArgs pe)
        {
            var graphics = pe.Graphics;
            Debug.WriteLine("OnPaint");
            var selected = (bool)FormUtil.ReadTag(this, "selected", false);
            var hovered = (bool) FormUtil.ReadTag(this, "hovered", false);
            var backColor = selected
                ? SettingsTabBackgroundSelected
                : hovered
                    ? SettingsTabBackgroundHovered
                    : ClientForm.Instance()?.SettingsView.settingsMenu.settingsContainerLeft.BackColor;
            Debug.WriteLine("Clearing " + backColor);
            graphics.Clear(backColor ?? Color.Black);

            AutoSize = false;
            Dock = DockStyle.Fill;
            Parent.Font = font;

            var circleSize = 24;
            graphics.SmoothingMode = SmoothingMode.AntiAlias;
            graphics.DrawEllipse(OrangePen, 20, 5, circleSize, circleSize);
            Debug.WriteLine("Drawing String font=" + Parent.Font);
            graphics.DrawString(Parent.Text, new Font(FormUtil.OpenSans, 10f), WhiteBrush, 20 + circleSize + 10, 7);

        }
    }
}
