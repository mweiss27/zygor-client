﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Zygor_Client.CustomControls;
using Zygor_Client.Util;

namespace Zygor_Client.UserControls
{
    public partial class SettingsTab : UserControl
    {

        #region Statics

        private static readonly Color SettingsTabBackgroundHovered = Color.FromArgb(255, 55, 55, 55);
        #endregion

        public SettingsTab()
        {
            InitializeComponent();
        }
        
        private void SettingsTab_MouseEnter(object sender, EventArgs e)
        {
            var tab = sender as SettingsTabLabel;
            if (tab != null)
            {
                FormUtil.SetTag(tab, "hovered", true);
                tab.Refresh();
            }
        }

        private void SettingsTab_MouseLeave(object sender, EventArgs e)
        {
            var tab = sender as SettingsTabLabel;
            if (tab != null)
            {
                FormUtil.SetTag(tab, "hovered", false);
                tab.Refresh();
            }
        }

        private void settingsTabLabel_Click(object sender, EventArgs e)
        {
            Debug.WriteLine("Click");
            var tab = sender as SettingsTabLabel;
            if (tab != null)
            {
                var label = tab.Text;
                var settingsMenu = FormUtil.GetFirstParentOfType(tab, typeof(SettingsMenu)) as SettingsMenu;
                settingsMenu?.SelectTab(label);
            }
        }
    }
}
