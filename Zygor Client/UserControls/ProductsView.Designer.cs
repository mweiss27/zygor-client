﻿using Zygor_Client.Util;

namespace Zygor_Client.UserControls
{
    partial class ProductsView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.mainContainerCenter = new System.Windows.Forms.Panel();
            this.historyLabel = new System.Windows.Forms.Label();
            this.mainHeaders = new System.Windows.Forms.Panel();
            this.actionCol5 = new System.Windows.Forms.Panel();
            this.statusLabel = new System.Windows.Forms.Label();
            this.actionCol4 = new System.Windows.Forms.Panel();
            this.latestLabel = new System.Windows.Forms.Label();
            this.actionCol3 = new System.Windows.Forms.Panel();
            this.installedLabel = new System.Windows.Forms.Label();
            this.actionCol2 = new System.Windows.Forms.Panel();
            this.productLabel = new System.Windows.Forms.Label();
            this.actionCol1 = new System.Windows.Forms.Panel();
            this.actionsSeparator = new System.Windows.Forms.Label();
            this.actionsContainer = new System.Windows.Forms.Panel();
            this.mainContainerCenter.SuspendLayout();
            this.mainHeaders.SuspendLayout();
            this.actionCol5.SuspendLayout();
            this.actionCol4.SuspendLayout();
            this.actionCol3.SuspendLayout();
            this.actionCol2.SuspendLayout();
            this.SuspendLayout();
            // 
            // mainContainerCenter
            // 
            this.mainContainerCenter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(32)))), ((int)(((byte)(32)))));
            this.mainContainerCenter.Controls.Add(this.historyLabel);
            this.mainContainerCenter.Controls.Add(this.mainHeaders);
            this.mainContainerCenter.Controls.Add(this.actionsSeparator);
            this.mainContainerCenter.Controls.Add(this.actionsContainer);
            this.mainContainerCenter.Location = new System.Drawing.Point(0, 0);
            this.mainContainerCenter.Margin = new System.Windows.Forms.Padding(0);
            this.mainContainerCenter.Name = "mainContainerCenter";
            this.mainContainerCenter.Size = new System.Drawing.Size(615, 413);
            this.mainContainerCenter.TabIndex = 3;
            // 
            // historyLabel
            // 
            this.historyLabel.AutoSize = true;
            this.historyLabel.BackColor = System.Drawing.Color.Transparent;
            this.historyLabel.Cursor = System.Windows.Forms.Cursors.Hand;
            this.historyLabel.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.historyLabel.Font = new System.Drawing.Font("Arial", 9F);
            this.historyLabel.ForeColor = System.Drawing.Color.OrangeRed;
            this.historyLabel.Location = new System.Drawing.Point(0, 392);
            this.historyLabel.Margin = new System.Windows.Forms.Padding(0);
            this.historyLabel.Name = "historyLabel";
            this.historyLabel.Padding = new System.Windows.Forms.Padding(8, 0, 0, 4);
            this.historyLabel.Size = new System.Drawing.Size(131, 21);
            this.historyLabel.TabIndex = 3;
            this.historyLabel.Text = "Download History";
            // 
            // mainHeaders
            // 
            this.mainHeaders.Controls.Add(this.actionCol5);
            this.mainHeaders.Controls.Add(this.actionCol4);
            this.mainHeaders.Controls.Add(this.actionCol3);
            this.mainHeaders.Controls.Add(this.actionCol2);
            this.mainHeaders.Controls.Add(this.actionCol1);
            this.mainHeaders.Dock = System.Windows.Forms.DockStyle.Top;
            this.mainHeaders.Location = new System.Drawing.Point(0, 33);
            this.mainHeaders.Margin = new System.Windows.Forms.Padding(2);
            this.mainHeaders.Name = "mainHeaders";
            this.mainHeaders.Size = new System.Drawing.Size(615, 37);
            this.mainHeaders.TabIndex = 2;
            // 
            // actionCol5
            // 
            this.actionCol5.BackColor = System.Drawing.Color.Transparent;
            this.actionCol5.Controls.Add(this.statusLabel);
            this.actionCol5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.actionCol5.Location = new System.Drawing.Point(499, 0);
            this.actionCol5.Margin = new System.Windows.Forms.Padding(2);
            this.actionCol5.Name = "actionCol5";
            this.actionCol5.Size = new System.Drawing.Size(116, 37);
            this.actionCol5.TabIndex = 5;
            // 
            // statusLabel
            // 
            this.statusLabel.BackColor = System.Drawing.Color.Transparent;
            this.statusLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.statusLabel.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.statusLabel.ForeColor = System.Drawing.Color.White;
            this.statusLabel.Location = new System.Drawing.Point(0, 0);
            this.statusLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.statusLabel.Name = "statusLabel";
            this.statusLabel.Size = new System.Drawing.Size(116, 37);
            this.statusLabel.TabIndex = 3;
            this.statusLabel.Text = "STATUS";
            this.statusLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // actionCol4
            // 
            this.actionCol4.BackColor = System.Drawing.Color.Transparent;
            this.actionCol4.Controls.Add(this.latestLabel);
            this.actionCol4.Dock = System.Windows.Forms.DockStyle.Left;
            this.actionCol4.Location = new System.Drawing.Point(349, 0);
            this.actionCol4.Margin = new System.Windows.Forms.Padding(2);
            this.actionCol4.Name = "actionCol4";
            this.actionCol4.Size = new System.Drawing.Size(150, 37);
            this.actionCol4.TabIndex = 4;
            // 
            // latestLabel
            // 
            this.latestLabel.BackColor = System.Drawing.Color.Transparent;
            this.latestLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.latestLabel.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.latestLabel.ForeColor = System.Drawing.Color.White;
            this.latestLabel.Location = new System.Drawing.Point(0, 0);
            this.latestLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.latestLabel.Name = "latestLabel";
            this.latestLabel.Size = new System.Drawing.Size(150, 37);
            this.latestLabel.TabIndex = 2;
            this.latestLabel.Text = "LATEST";
            this.latestLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // actionCol3
            // 
            this.actionCol3.BackColor = System.Drawing.Color.Transparent;
            this.actionCol3.Controls.Add(this.installedLabel);
            this.actionCol3.Dock = System.Windows.Forms.DockStyle.Left;
            this.actionCol3.Location = new System.Drawing.Point(199, 0);
            this.actionCol3.Margin = new System.Windows.Forms.Padding(2);
            this.actionCol3.Name = "actionCol3";
            this.actionCol3.Size = new System.Drawing.Size(150, 37);
            this.actionCol3.TabIndex = 3;
            // 
            // installedLabel
            // 
            this.installedLabel.BackColor = System.Drawing.Color.Transparent;
            this.installedLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.installedLabel.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.installedLabel.ForeColor = System.Drawing.Color.White;
            this.installedLabel.Location = new System.Drawing.Point(0, 0);
            this.installedLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.installedLabel.Name = "installedLabel";
            this.installedLabel.Size = new System.Drawing.Size(150, 37);
            this.installedLabel.TabIndex = 1;
            this.installedLabel.Text = "INSTALLED";
            this.installedLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // actionCol2
            // 
            this.actionCol2.BackColor = System.Drawing.Color.Transparent;
            this.actionCol2.Controls.Add(this.productLabel);
            this.actionCol2.Dock = System.Windows.Forms.DockStyle.Left;
            this.actionCol2.Location = new System.Drawing.Point(49, 0);
            this.actionCol2.Margin = new System.Windows.Forms.Padding(2);
            this.actionCol2.Name = "actionCol2";
            this.actionCol2.Size = new System.Drawing.Size(150, 37);
            this.actionCol2.TabIndex = 2;
            // 
            // productLabel
            // 
            this.productLabel.BackColor = System.Drawing.Color.Transparent;
            this.productLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.productLabel.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.productLabel.ForeColor = System.Drawing.Color.White;
            this.productLabel.Location = new System.Drawing.Point(0, 0);
            this.productLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.productLabel.Name = "productLabel";
            this.productLabel.Size = new System.Drawing.Size(150, 37);
            this.productLabel.TabIndex = 0;
            this.productLabel.Text = "PRODUCT";
            this.productLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // actionCol1
            // 
            this.actionCol1.BackColor = System.Drawing.Color.Transparent;
            this.actionCol1.Dock = System.Windows.Forms.DockStyle.Left;
            this.actionCol1.Location = new System.Drawing.Point(0, 0);
            this.actionCol1.Margin = new System.Windows.Forms.Padding(2);
            this.actionCol1.Name = "actionCol1";
            this.actionCol1.Size = new System.Drawing.Size(49, 37);
            this.actionCol1.TabIndex = 1;
            // 
            // actionsSeparator
            // 
            this.actionsSeparator.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.actionsSeparator.Dock = System.Windows.Forms.DockStyle.Top;
            this.actionsSeparator.Location = new System.Drawing.Point(0, 32);
            this.actionsSeparator.Margin = new System.Windows.Forms.Padding(0);
            this.actionsSeparator.Name = "actionsSeparator";
            this.actionsSeparator.Size = new System.Drawing.Size(615, 1);
            this.actionsSeparator.TabIndex = 1;
            // 
            // actionsContainer
            // 
            this.actionsContainer.Dock = System.Windows.Forms.DockStyle.Top;
            this.actionsContainer.Location = new System.Drawing.Point(0, 0);
            this.actionsContainer.Margin = new System.Windows.Forms.Padding(2);
            this.actionsContainer.Name = "actionsContainer";
            this.actionsContainer.Size = new System.Drawing.Size(615, 32);
            this.actionsContainer.TabIndex = 0;
            // 
            // ProductsView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.mainContainerCenter);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "ProductsView";
            this.Size = new System.Drawing.Size(615, 413);
            this.mainContainerCenter.ResumeLayout(false);
            this.mainContainerCenter.PerformLayout();
            this.mainHeaders.ResumeLayout(false);
            this.actionCol5.ResumeLayout(false);
            this.actionCol4.ResumeLayout(false);
            this.actionCol3.ResumeLayout(false);
            this.actionCol2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        public System.Windows.Forms.Panel mainContainerCenter;
        public System.Windows.Forms.Label historyLabel;
        public System.Windows.Forms.Panel mainHeaders;
        public System.Windows.Forms.Panel actionCol5;
        public System.Windows.Forms.Label statusLabel;
        public System.Windows.Forms.Panel actionCol4;
        public System.Windows.Forms.Label latestLabel;
        public System.Windows.Forms.Panel actionCol3;
        public System.Windows.Forms.Label installedLabel;
        public System.Windows.Forms.Panel actionCol2;
        public System.Windows.Forms.Label productLabel;
        public System.Windows.Forms.Panel actionCol1;
        public System.Windows.Forms.Label actionsSeparator;
        public System.Windows.Forms.Panel actionsContainer;
    }
}
