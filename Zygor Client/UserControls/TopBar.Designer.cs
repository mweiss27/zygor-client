﻿using System;
using System.Drawing;
using Zygor_Client.Util;

namespace Zygor_Client.UserControls
{
    partial class TopBar
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        public System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.topPanel = new System.Windows.Forms.Panel();
            this.minimizeButtonContainer = new System.Windows.Forms.Panel();
            this.minimizeImage = new System.Windows.Forms.PictureBox();
            this.closeButtonContainer = new System.Windows.Forms.Panel();
            this.closeImage = new System.Windows.Forms.PictureBox();
            this.topLeftFlowPanel = new System.Windows.Forms.FlowLayoutPanel();
            this.logoImage = new System.Windows.Forms.PictureBox();
            this.guidesLabel = new System.Windows.Forms.Label();
            this.newsLabel = new System.Windows.Forms.Label();
            this.topPanel.SuspendLayout();
            this.minimizeButtonContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.minimizeImage)).BeginInit();
            this.closeButtonContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.closeImage)).BeginInit();
            this.topLeftFlowPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.logoImage)).BeginInit();
            this.SuspendLayout();
            // 
            // topPanel
            // 
            this.topPanel.BackColor = System.Drawing.Color.Black;
            this.topPanel.Controls.Add(this.minimizeButtonContainer);
            this.topPanel.Controls.Add(this.closeButtonContainer);
            this.topPanel.Controls.Add(this.topLeftFlowPanel);
            this.topPanel.Location = new System.Drawing.Point(0, 0);
            this.topPanel.Margin = new System.Windows.Forms.Padding(0);
            this.topPanel.Name = "topPanel";
            this.topPanel.Size = new System.Drawing.Size(824, 37);
            this.topPanel.TabIndex = 1;
            this.topPanel.MouseDown += new System.Windows.Forms.MouseEventHandler(this.topPanel_MouseDown);
            // 
            // minimizeButtonContainer
            // 
            this.minimizeButtonContainer.AutoScroll = true;
            this.minimizeButtonContainer.BackColor = System.Drawing.Color.Transparent;
            this.minimizeButtonContainer.Controls.Add(this.minimizeImage);
            this.minimizeButtonContainer.Location = new System.Drawing.Point(776, 0);
            this.minimizeButtonContainer.Margin = new System.Windows.Forms.Padding(0);
            this.minimizeButtonContainer.Name = "minimizeButtonContainer";
            this.minimizeButtonContainer.Size = new System.Drawing.Size(24, 26);
            this.minimizeButtonContainer.TabIndex = 5;
            this.minimizeButtonContainer.MouseClick += new System.Windows.Forms.MouseEventHandler(this.minimizeButtonContainer_MouseClick);
            // 
            // minimizeImage
            // 
            this.minimizeImage.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.minimizeImage.Enabled = false;
            this.minimizeImage.Image = global::Zygor_Client.Properties.Resources.minimize128;
            this.minimizeImage.Location = new System.Drawing.Point(3, 3);
            this.minimizeImage.Margin = new System.Windows.Forms.Padding(2);
            this.minimizeImage.Name = "minimizeImage";
            this.minimizeImage.Size = new System.Drawing.Size(18, 20);
            this.minimizeImage.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.minimizeImage.TabIndex = 3;
            this.minimizeImage.TabStop = false;
            // 
            // closeButtonContainer
            // 
            this.closeButtonContainer.AutoScroll = true;
            this.closeButtonContainer.BackColor = System.Drawing.Color.Transparent;
            this.closeButtonContainer.Controls.Add(this.closeImage);
            this.closeButtonContainer.Location = new System.Drawing.Point(800, 0);
            this.closeButtonContainer.Margin = new System.Windows.Forms.Padding(0);
            this.closeButtonContainer.Name = "closeButtonContainer";
            this.closeButtonContainer.Size = new System.Drawing.Size(24, 26);
            this.closeButtonContainer.TabIndex = 4;
            this.closeButtonContainer.MouseClick += new System.Windows.Forms.MouseEventHandler(this.closeButtonContainer_MouseClick);
            // 
            // closeImage
            // 
            this.closeImage.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.closeImage.BackColor = System.Drawing.Color.Transparent;
            this.closeImage.Enabled = false;
            this.closeImage.Image = global::Zygor_Client.Properties.Resources.close128;
            this.closeImage.Location = new System.Drawing.Point(3, 3);
            this.closeImage.Margin = new System.Windows.Forms.Padding(0);
            this.closeImage.Name = "closeImage";
            this.closeImage.Size = new System.Drawing.Size(18, 20);
            this.closeImage.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.closeImage.TabIndex = 2;
            this.closeImage.TabStop = false;
            // 
            // topLeftFlowPanel
            // 
            this.topLeftFlowPanel.Controls.Add(this.logoImage);
            this.topLeftFlowPanel.Controls.Add(this.guidesLabel);
            this.topLeftFlowPanel.Controls.Add(this.newsLabel);
            this.topLeftFlowPanel.Dock = System.Windows.Forms.DockStyle.Left;
            this.topLeftFlowPanel.Location = new System.Drawing.Point(0, 0);
            this.topLeftFlowPanel.Margin = new System.Windows.Forms.Padding(2);
            this.topLeftFlowPanel.Name = "topLeftFlowPanel";
            this.topLeftFlowPanel.Size = new System.Drawing.Size(375, 37);
            this.topLeftFlowPanel.TabIndex = 0;
            this.topLeftFlowPanel.MouseDown += new System.Windows.Forms.MouseEventHandler(this.topPanel_MouseDown);
            // 
            // logoImage
            // 
            this.logoImage.Image = global::Zygor_Client.Properties.Resources.logo;
            this.logoImage.Location = new System.Drawing.Point(2, 2);
            this.logoImage.Margin = new System.Windows.Forms.Padding(2);
            this.logoImage.Name = "logoImage";
            this.logoImage.Size = new System.Drawing.Size(90, 28);
            this.logoImage.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.logoImage.TabIndex = 0;
            this.logoImage.TabStop = false;
            this.logoImage.MouseDown += new System.Windows.Forms.MouseEventHandler(this.topPanel_MouseDown);
            // 
            // guidesLabel
            // 
            this.guidesLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.guidesLabel.AutoSize = true;
            this.guidesLabel.BackColor = System.Drawing.Color.Transparent;
            this.guidesLabel.Font = new System.Drawing.Font("Arial", 15F);
            this.guidesLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(200)))), ((int)(((byte)(200)))));
            this.guidesLabel.Location = new System.Drawing.Point(116, 0);
            this.guidesLabel.Margin = new System.Windows.Forms.Padding(22, 0, 0, 0);
            this.guidesLabel.Name = "guidesLabel";
            this.guidesLabel.Size = new System.Drawing.Size(64, 32);
            this.guidesLabel.TabIndex = 1;
            this.guidesLabel.Text = "Home";
            this.guidesLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // newsLabel
            // 
            this.newsLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.newsLabel.AutoSize = true;
            this.newsLabel.BackColor = System.Drawing.Color.Transparent;
            this.newsLabel.Font = new System.Drawing.Font("Arial", 15F);
            this.newsLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.newsLabel.Location = new System.Drawing.Point(202, 0);
            this.newsLabel.Margin = new System.Windows.Forms.Padding(22, 0, 2, 0);
            this.newsLabel.Name = "newsLabel";
            this.newsLabel.Size = new System.Drawing.Size(64, 32);
            this.newsLabel.TabIndex = 0;
            this.newsLabel.Text = "News";
            this.newsLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // TopBar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.topPanel);
            this.Margin = new System.Windows.Forms.Padding(0);
            this.Name = "TopBar";
            this.Size = new System.Drawing.Size(824, 37);
            this.topPanel.ResumeLayout(false);
            this.minimizeButtonContainer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.minimizeImage)).EndInit();
            this.closeButtonContainer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.closeImage)).EndInit();
            this.topLeftFlowPanel.ResumeLayout(false);
            this.topLeftFlowPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.logoImage)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        public System.Windows.Forms.Panel topPanel;
        public System.Windows.Forms.Panel minimizeButtonContainer;
        public System.Windows.Forms.PictureBox minimizeImage;
        public System.Windows.Forms.Panel closeButtonContainer;
        public System.Windows.Forms.PictureBox closeImage;
        public System.Windows.Forms.FlowLayoutPanel topLeftFlowPanel;
        public System.Windows.Forms.PictureBox logoImage;
        public System.Windows.Forms.Label guidesLabel;
        public System.Windows.Forms.Label newsLabel;
    }
}
