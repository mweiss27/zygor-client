﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Text;
using System.Windows.Forms;
using Zygor_Client.Util;

namespace Zygor_Client.UserControls
{
    partial class AccountOverview
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        public void InitializeComponent()
        {
            this.mainContainerLeft = new System.Windows.Forms.FlowLayoutPanel();
            this.accountNameValue = new System.Windows.Forms.Label();
            this.avatarImage = new System.Windows.Forms.PictureBox();
            this.membershipLabel = new System.Windows.Forms.Label();
            this.membershipValue = new System.Windows.Forms.Label();
            this.subscriptionLabel = new System.Windows.Forms.Label();
            this.subscriptionValue = new System.Windows.Forms.Label();
            this.editAccountLabel = new System.Windows.Forms.Label();
            this.supportLabel = new System.Windows.Forms.Label();
            this.logoutLabel = new System.Windows.Forms.Label();
            this.mainContainerLeft.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.avatarImage)).BeginInit();
            this.SuspendLayout();
            // 
            // mainContainerLeft
            // 
            this.mainContainerLeft.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(48)))), ((int)(((byte)(48)))));
            this.mainContainerLeft.Controls.Add(this.accountNameValue);
            this.mainContainerLeft.Controls.Add(this.avatarImage);
            this.mainContainerLeft.Controls.Add(this.membershipLabel);
            this.mainContainerLeft.Controls.Add(this.membershipValue);
            this.mainContainerLeft.Controls.Add(this.subscriptionLabel);
            this.mainContainerLeft.Controls.Add(this.subscriptionValue);
            this.mainContainerLeft.Controls.Add(this.editAccountLabel);
            this.mainContainerLeft.Controls.Add(this.supportLabel);
            this.mainContainerLeft.Controls.Add(this.logoutLabel);
            this.mainContainerLeft.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.mainContainerLeft.Location = new System.Drawing.Point(0, 0);
            this.mainContainerLeft.Margin = new System.Windows.Forms.Padding(0);
            this.mainContainerLeft.Name = "mainContainerLeft";
            this.mainContainerLeft.Size = new System.Drawing.Size(280, 508);
            this.mainContainerLeft.TabIndex = 4;
            // 
            // accountNameValue
            // 
            this.accountNameValue.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.accountNameValue.AutoSize = true;
            this.accountNameValue.BackColor = System.Drawing.Color.Transparent;
            this.accountNameValue.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold);
            this.accountNameValue.ForeColor = System.Drawing.Color.White;
            this.accountNameValue.Location = new System.Drawing.Point(46, 35);
            this.accountNameValue.Margin = new System.Windows.Forms.Padding(3, 35, 3, 0);
            this.accountNameValue.Name = "accountNameValue";
            this.accountNameValue.Size = new System.Drawing.Size(191, 33);
            this.accountNameValue.TabIndex = 0;
            this.accountNameValue.Text = "Account Name";
            this.accountNameValue.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // avatarImage
            // 
            this.avatarImage.BackColor = System.Drawing.Color.Transparent;
            this.avatarImage.Location = new System.Drawing.Point(74, 71);
            this.avatarImage.Margin = new System.Windows.Forms.Padding(74, 3, 3, 3);
            this.avatarImage.Name = "avatarImage";
            this.avatarImage.Size = new System.Drawing.Size(125, 125);
            this.avatarImage.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.avatarImage.TabIndex = 1;
            this.avatarImage.TabStop = false;
            // 
            // membershipLabel
            // 
            this.membershipLabel.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.membershipLabel.AutoSize = true;
            this.membershipLabel.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.membershipLabel.ForeColor = System.Drawing.Color.White;
            this.membershipLabel.Location = new System.Drawing.Point(70, 219);
            this.membershipLabel.Margin = new System.Windows.Forms.Padding(3, 20, 3, 0);
            this.membershipLabel.Name = "membershipLabel";
            this.membershipLabel.Size = new System.Drawing.Size(143, 20);
            this.membershipLabel.TabIndex = 2;
            this.membershipLabel.Text = "Membership Type";
            this.membershipLabel.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // membershipValue
            // 
            this.membershipValue.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.membershipValue.AutoSize = true;
            this.membershipValue.Font = new System.Drawing.Font("Arial", 9F);
            this.membershipValue.ForeColor = System.Drawing.Color.White;
            this.membershipValue.Location = new System.Drawing.Point(122, 239);
            this.membershipValue.Name = "membershipValue";
            this.membershipValue.Size = new System.Drawing.Size(38, 20);
            this.membershipValue.TabIndex = 3;
            this.membershipValue.Text = "Elite";
            this.membershipValue.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // subscriptionLabel
            // 
            this.subscriptionLabel.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.subscriptionLabel.AutoSize = true;
            this.subscriptionLabel.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.subscriptionLabel.ForeColor = System.Drawing.Color.White;
            this.subscriptionLabel.Location = new System.Drawing.Point(90, 284);
            this.subscriptionLabel.Margin = new System.Windows.Forms.Padding(3, 25, 3, 0);
            this.subscriptionLabel.Name = "subscriptionLabel";
            this.subscriptionLabel.Size = new System.Drawing.Size(103, 20);
            this.subscriptionLabel.TabIndex = 4;
            this.subscriptionLabel.Text = "Subscription";
            this.subscriptionLabel.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // subscriptionValue
            // 
            this.subscriptionValue.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.subscriptionValue.AutoSize = true;
            this.subscriptionValue.Font = new System.Drawing.Font("Arial", 9F);
            this.subscriptionValue.ForeColor = System.Drawing.Color.White;
            this.subscriptionValue.Location = new System.Drawing.Point(116, 304);
            this.subscriptionValue.Name = "subscriptionValue";
            this.subscriptionValue.Size = new System.Drawing.Size(50, 20);
            this.subscriptionValue.TabIndex = 5;
            this.subscriptionValue.Text = "Active";
            this.subscriptionValue.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // editAccountLabel
            // 
            this.editAccountLabel.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.editAccountLabel.AutoSize = true;
            this.editAccountLabel.BackColor = System.Drawing.Color.Transparent;
            this.editAccountLabel.Cursor = System.Windows.Forms.Cursors.Hand;
            this.editAccountLabel.Font = new System.Drawing.Font("Arial", 9F);
            this.editAccountLabel.ForeColor = System.Drawing.Color.OrangeRed;
            this.editAccountLabel.Location = new System.Drawing.Point(94, 359);
            this.editAccountLabel.Margin = new System.Windows.Forms.Padding(3, 35, 3, 0);
            this.editAccountLabel.Name = "editAccountLabel";
            this.editAccountLabel.Size = new System.Drawing.Size(94, 20);
            this.editAccountLabel.TabIndex = 6;
            this.editAccountLabel.Text = "Edit Account";
            this.editAccountLabel.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // supportLabel
            // 
            this.supportLabel.Cursor = System.Windows.Forms.Cursors.Hand;
            this.supportLabel.Font = new System.Drawing.Font("Arial", 9F);
            this.supportLabel.ForeColor = System.Drawing.Color.OrangeRed;
            this.supportLabel.Location = new System.Drawing.Point(3, 382);
            this.supportLabel.Margin = new System.Windows.Forms.Padding(3, 3, 3, 0);
            this.supportLabel.Name = "supportLabel";
            this.supportLabel.Size = new System.Drawing.Size(277, 23);
            this.supportLabel.TabIndex = 7;
            this.supportLabel.Text = "Support";
            this.supportLabel.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // logoutLabel
            // 
            this.logoutLabel.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.logoutLabel.AutoSize = true;
            this.logoutLabel.Cursor = System.Windows.Forms.Cursors.Hand;
            this.logoutLabel.Font = new System.Drawing.Font("Arial", 9F);
            this.logoutLabel.ForeColor = System.Drawing.Color.OrangeRed;
            this.logoutLabel.Location = new System.Drawing.Point(113, 408);
            this.logoutLabel.Margin = new System.Windows.Forms.Padding(3, 3, 3, 0);
            this.logoutLabel.Name = "logoutLabel";
            this.logoutLabel.Size = new System.Drawing.Size(57, 20);
            this.logoutLabel.TabIndex = 8;
            this.logoutLabel.Text = "Logout";
            this.logoutLabel.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // AccountOverview
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.Controls.Add(this.mainContainerLeft);
            this.Name = "AccountOverview";
            this.Size = new System.Drawing.Size(280, 508);
            this.mainContainerLeft.ResumeLayout(false);
            this.mainContainerLeft.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.avatarImage)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        public FlowLayoutPanel mainContainerLeft;
        public Label accountNameValue;
        public PictureBox avatarImage;
        public Label membershipLabel;
        public Label membershipValue;
        public Label subscriptionLabel;
        public Label subscriptionValue;
        public Label editAccountLabel;
        public Label supportLabel;
        public Label logoutLabel;
    }
}
