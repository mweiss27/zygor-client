﻿using Zygor_Client.Util;

namespace Zygor_Client.UserControls
{
    partial class BottomBar
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.versionValue = new System.Windows.Forms.Label();
            this.versionLabel = new System.Windows.Forms.Label();
            this.bottomRightContainer = new System.Windows.Forms.FlowLayoutPanel();
            this.settingsButtonContainer = new System.Windows.Forms.Panel();
            this.settingsImage = new System.Windows.Forms.PictureBox();
            this.bottomRightContainer.SuspendLayout();
            this.settingsButtonContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.settingsImage)).BeginInit();
            this.SuspendLayout();
            // 
            // versionValue
            // 
            this.versionValue.AutoSize = true;
            this.versionValue.BackColor = System.Drawing.Color.Transparent;
            this.versionValue.Font = new System.Drawing.Font("Arial Black", 10F);
            this.versionValue.ForeColor = System.Drawing.Color.White;
            this.versionValue.Location = new System.Drawing.Point(31, 9);
            this.versionValue.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.versionValue.Name = "versionValue";
            this.versionValue.Size = new System.Drawing.Size(60, 19);
            this.versionValue.TabIndex = 1;
            this.versionValue.Text = "4.0.0.1";
            this.versionValue.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // versionLabel
            // 
            this.versionLabel.BackColor = System.Drawing.Color.Transparent;
            this.versionLabel.Dock = System.Windows.Forms.DockStyle.Left;
            this.versionLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold);
            this.versionLabel.ForeColor = System.Drawing.Color.White;
            this.versionLabel.Location = new System.Drawing.Point(0, 0);
            this.versionLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.versionLabel.Name = "versionLabel";
            this.versionLabel.Size = new System.Drawing.Size(46, 37);
            this.versionLabel.TabIndex = 0;
            this.versionLabel.Text = "VER: ";
            this.versionLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // bottomRightContainer
            // 
            this.bottomRightContainer.Controls.Add(this.settingsButtonContainer);
            this.bottomRightContainer.Dock = System.Windows.Forms.DockStyle.Right;
            this.bottomRightContainer.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft;
            this.bottomRightContainer.Location = new System.Drawing.Point(563, 0);
            this.bottomRightContainer.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.bottomRightContainer.Name = "bottomRightContainer";
            this.bottomRightContainer.Size = new System.Drawing.Size(262, 37);
            this.bottomRightContainer.TabIndex = 3;
            // 
            // settingsButtonContainer
            // 
            this.settingsButtonContainer.Controls.Add(this.settingsImage);
            this.settingsButtonContainer.Location = new System.Drawing.Point(233, 4);
            this.settingsButtonContainer.Margin = new System.Windows.Forms.Padding(0, 4, 8, 0);
            this.settingsButtonContainer.Name = "settingsButtonContainer";
            this.settingsButtonContainer.Size = new System.Drawing.Size(21, 23);
            this.settingsButtonContainer.TabIndex = 5;
            // 
            // settingsImage
            // 
            this.settingsImage.BackColor = System.Drawing.Color.Transparent;
            this.settingsImage.Cursor = System.Windows.Forms.Cursors.Hand;
            this.settingsImage.Image = global::Zygor_Client.Properties.Resources.settings128;
            this.settingsImage.Location = new System.Drawing.Point(0, 0);
            this.settingsImage.Margin = new System.Windows.Forms.Padding(0);
            this.settingsImage.Name = "settingsImage";
            this.settingsImage.Size = new System.Drawing.Size(21, 23);
            this.settingsImage.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.settingsImage.TabIndex = 4;
            this.settingsImage.TabStop = false;
            this.settingsImage.MouseClick += new System.Windows.Forms.MouseEventHandler(this.settingsImage_MouseClick);
            // 
            // BottomBar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.Controls.Add(this.bottomRightContainer);
            this.Controls.Add(this.versionValue);
            this.Controls.Add(this.versionLabel);
            this.Margin = new System.Windows.Forms.Padding(0);
            this.Name = "BottomBar";
            this.Size = new System.Drawing.Size(825, 37);
            this.bottomRightContainer.ResumeLayout(false);
            this.settingsButtonContainer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.settingsImage)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label versionValue;
        private System.Windows.Forms.Label versionLabel;
        private System.Windows.Forms.PictureBox settingsImage;
        private System.Windows.Forms.FlowLayoutPanel bottomRightContainer;
        private System.Windows.Forms.Panel settingsButtonContainer;
    }
}
