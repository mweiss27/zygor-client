﻿namespace Zygor_Client.UserControls
{
    partial class SettingsView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.settingsMenu = new SettingsMenu();
            this.settingsTabView = new SettingsTabView();
            this.SuspendLayout();
            // 
            // settingsMenu
            // 
            this.settingsMenu.BackColor = System.Drawing.Color.Black;
            this.settingsMenu.Dock = System.Windows.Forms.DockStyle.Left;
            this.settingsMenu.Location = new System.Drawing.Point(0, 0);
            this.settingsMenu.Margin = new System.Windows.Forms.Padding(0);
            this.settingsMenu.Name = "settingsMenu";
            this.settingsMenu.Size = new System.Drawing.Size(280, 508);
            this.settingsMenu.TabIndex = 0;
            // 
            // settingsTabView
            // 
            this.settingsTabView.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(32)))), ((int)(((byte)(32)))));
            this.settingsTabView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.settingsTabView.Location = new System.Drawing.Point(280, 0);
            this.settingsTabView.Margin = new System.Windows.Forms.Padding(0);
            this.settingsTabView.Name = "settingsTabView";
            this.settingsTabView.Size = new System.Drawing.Size(820, 508);
            this.settingsTabView.TabIndex = 1;
            // 
            // SettingsView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.settingsTabView);
            this.Controls.Add(this.settingsMenu);
            this.Name = "SettingsView";
            this.Size = new System.Drawing.Size(1100, 508);
            this.ResumeLayout(false);

        }

        #endregion

        public SettingsMenu settingsMenu;
        public SettingsTabView settingsTabView;
    }
}
