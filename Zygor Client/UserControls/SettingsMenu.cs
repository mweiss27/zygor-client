﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using Zygor_Client.CustomControls;
using Zygor_Client.Util;

namespace Zygor_Client.UserControls
{
    public partial class SettingsMenu : UserControl
    {

        private static readonly Color SettingsTabContentViewBackColor = Color.FromArgb(255, 32, 32, 32);
        private static readonly Color SettingsTabBackgroundSelected = Color.FromArgb(255, 85, 85, 85);

        public SettingsMenu()
        {
            InitializeComponent();

            try
            {
                this.ParseSettingsXml();
            }
            catch (Exception ignored)
            {
                
            }

        }

        private void ParseSettingsXml()
        {
            var doc= new XmlDocument();
            doc.Load("Resources/Views/Settings.xml");

            var viewNode = doc.FirstChild;
            if (viewNode.Name == "xml")
                viewNode = viewNode.NextSibling;

            if (viewNode != null)
            {
                if (viewNode.Name == "view")
                {
                    var children = viewNode.ChildNodes;
                    for (var i = 0; i < children.Count; i++)
                    {
                        var tab = children[i];
                        if (tab.Name == "settingsTab")
                        {
                            var settingsTab = new SettingsTab();
                            var tabProperties = tab.ChildNodes;
                            for (var j = 0; j < tabProperties.Count; j++)
                            {
                                var property = tabProperties[j];
                                if (property.Name == "id")
                                    settingsTab.Name = property.InnerText;

                                else if (property.Name == "name")
                                    settingsTab.Text = property.InnerText;

                                else if (property.Name == "content")
                                {

                                    var elements = property.ChildNodes;
                                    if (elements.Count != 1)
                                    {
                                        Debug.WriteLine("[ERROR] <content> can only contain one element. Use a <flow> node and place your elements with in");
                                        continue;
                                    }
                                    
                                    //ParseComponents will recurse when it hits a <flow> node.
                                    //The first node should be a flow
                                    FormUtil.SetTag(settingsTab, "view", ParseComponent(elements.Item(0)));
                                }
                            }
                            this.settingsContainerLeft.Controls.Add(settingsTab);


                        }
                        else
                            Debug.WriteLine("Invalid xml. Child of <view> is not <settingsTab>");
                    }


                }
                else
                    Debug.WriteLine("Invalid xml. <view> is not the root");
            }
            else
                Debug.WriteLine("Invalid xml. <view> not found");

        }

        private void AddChildrenToContainer(Control container, XmlNodeList components)
        {
            for (var k = 0; k < components.Count; k++)
            {
                var component = ParseComponent(components[k]);
                if (component != null)
                    container.Controls.Add(component);
                else
                    Debug.WriteLine($"[ERROR] Invalid xml. Component not recognized for type: {components[k].Name}");

            }
        }

        private Control ParseComponent(XmlNode node)
        {
            var type = node.Name.ToLower();
            Debug.WriteLine($"Parsing a component of type {type}");

            Control result = null;
            XmlNodeList options;
            switch (type)
            {
                case "flow":
                    result = new FlowLayoutPanel();
                    var children = node.ChildNodes;
                    AddChildrenToContainer(result, children);
                    break;
                case "label":
                    result = new Label()
                    {
                        AutoSize = true
                    };
                    break;
                case "checkbox":
                    result = new ZCheckBox()
                    {
                        AutoSize = true
                    };
                    break;
                case "radiobuttongroup":
                    options = node.ChildNodes;
                    Debug.WriteLine($"Parsing a radiobuttongroup with {options.Count} options");

                    var container = new FlowLayoutPanel()
                    {
                        AutoSize = true,
                        FlowDirection = FlowDirection.TopDown,
                        BackColor = Color.Transparent
                    };

                    for (var i = 0; i < options.Count; i++)
                    {
                        var option = options[i];
                        var button = new ZRadioButton()
                        {
                            AutoSize = true,
                            ForeColor = Color.White,
                            Text = option.InnerText,
                            Margin = new Padding(0),
                            BackColor = SettingsTabContentViewBackColor
                        };

                        container.Controls.Add(button);
                    }
                    result = container;

                    break;
                case "combobox":

                    options = node.ChildNodes;
                    Debug.WriteLine($"Parsing a combobox with {options.Count} options");
                    var cb = new ZComboBox();

                    var list = new List<string>();
                    for (var i = 0; i < options.Count; i++)
                    {
                        var option = options[i];
                        list.Add(option.InnerText);

                    }
                    cb.DataSource = list;
                    if (list.Count > 0)
                        cb.SelectedItem = list[0];

                    result = cb;
                    break;
            }

            if (result != null)
                ConfigureControl(result, node.Attributes);
            return result;
        }

        private void ConfigureControl(Control control, XmlAttributeCollection attributes)
        {
            control.ForeColor = Color.White; // This will get overidden below if they want to
            control.Font = new Font("OpenSans-Regular", 10f);

            for (var i = 0; i < attributes.Count; i++)
            {
                var attribute = attributes[i];
                var name = attribute.Name;
                if (name == "id")
                    control.Name = attribute.Value;

                else if (name == "text")
                    control.Text = attribute.Value;

                else if (name == "font")
                {
                    var tokens = attribute.Value.Split(',');
                    var fontFamily = tokens.Length >= 1 ? tokens[0] : "Open Sans";
                    var fontSize = tokens.Length >= 2 ? float.Parse(tokens[1]) : 12f;
                    control.Font = new Font(fontFamily, fontSize);
                }

                else if (name == "margin")
                {
                    var tokens = attribute.Value.Split(' ');
                    int top, left, bottom, right;
                    if (tokens.Length == 1)
                        left = top = right = bottom = int.Parse(tokens[0]);

                    else if (tokens.Length == 2)
                    {
                        left = right = int.Parse(tokens[0]);
                        top = bottom = int.Parse(tokens[1]);
                    }
                    
                    else if (tokens.Length == 4)
                    {
                        left = int.Parse(tokens[0]);
                        top = int.Parse(tokens[1]);
                        right = int.Parse(tokens[2]);
                        bottom = int.Parse(tokens[3]);
                    }
                    else
                    {
                        Debug.WriteLine("[ERROR] Invalid margin. Must be 1, 2, or 4 values. Top Left Bottom Right");
                        continue;
                    }

                    control.Margin = new Padding(left, top, right, bottom);
                }

                else if (name == "dock")
                    switch (attribute.Value)
                    {
                        case "fill":
                            control.Dock = DockStyle.Fill;
                            break;
                        case "left":
                            control.Dock = DockStyle.Left;
                            break;
                        case "top":
                            control.Dock = DockStyle.Top;
                            break;
                        case "bottom":
                            control.Dock = DockStyle.Bottom;
                            break;
                        case "right":
                            control.Dock = DockStyle.Right;
                            break;
                    }

                else if (name == "backcolor")
                    control.BackColor = ColorTranslator.FromHtml(attribute.Value);

                else if (name == "dir")
                {
                    var flow = (control as FlowLayoutPanel);
                    if (flow != null)
                        switch (attribute.Value)
                        {
                            case "td":
                                flow.FlowDirection = FlowDirection.TopDown;
                                break;
                            case "bu":
                                flow.FlowDirection = FlowDirection.BottomUp;
                                break;
                            case "ltr":
                                flow.FlowDirection = FlowDirection.LeftToRight;
                                break;
                            case "rtl":
                                flow.FlowDirection = FlowDirection.RightToLeft;
                                break;
                        }
                } 

                Debug.WriteLine($"Configuring attribute: {attribute.Name}");
            }
            
        }

        public void SelectTab(string selected)
        {
            Debug.WriteLine("SelectTab(" + selected + ")");
            var tabs = this.settingsContainerLeft.Controls;

            foreach (var tab in tabs)
            {
                var settingsTab = tab as SettingsTab;
                if (settingsTab != null)
                {
                    Debug.WriteLine("Checking " + settingsTab.Text);
                    if (settingsTab.Text == selected)
                    {
                        FormUtil.SetTag(settingsTab.settingsTabLabel, "selected", true);
                        settingsTab.Refresh();

                        var settingsView = this.Parent as SettingsView;
                        if (settingsView != null)
                        {
                            var view = FormUtil.ReadTag(settingsTab, "view", null) as Control;
                            if (view != null)
                            {
                                settingsView.settingsTabView.Controls.Clear();
                                settingsView.settingsTabView.Controls.Add(view);
                            }
                        }
                        else
                            Debug.WriteLine("settingsView is null. Correct parent?");
                    }
                    else
                    {
                        FormUtil.SetTag(settingsTab.settingsTabLabel, "selected", false);
                        settingsTab.Refresh();
                    }
                }
            }
        }
    }
}
