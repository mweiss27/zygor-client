﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Zygor_Client.UserControls
{
    public partial class SettingsView : UserControl
    {
        public SettingsView()
        {
            InitializeComponent();

            this.settingsMenu.SelectTab((this.settingsMenu.settingsContainerLeft.Controls[0] as SettingsTab)?.Text);
        }
    }
}
