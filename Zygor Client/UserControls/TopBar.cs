﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Zygor_Client.UserControls
{
    public partial class TopBar : UserControl
    {

        public const int WM_NCLBUTTONDOWN = 0xA1;
        public const int HT_CAPTION = 0x2;

        [System.Runtime.InteropServices.DllImportAttribute("user32.dll")]
        public static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, int lParam);

        [System.Runtime.InteropServices.DllImportAttribute("user32.dll")]
        public static extern bool ReleaseCapture();

        public TopBar()
        {
            InitializeComponent();
        }
        
        private void topPanel_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                ReleaseCapture();
                SendMessage(ClientForm.Instance().Handle, WM_NCLBUTTONDOWN, HT_CAPTION, 0);
            }
        }

        private void minimizeButtonContainer_MouseClick(object sender, MouseEventArgs e)
        {
            var form = ClientForm.Instance();
            form.Hide();
        }

        private void closeButtonContainer_MouseClick(object sender, MouseEventArgs e)
        {
            Program.Shutdown(sender, e);
        }
    }
}
