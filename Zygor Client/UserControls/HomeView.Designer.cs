﻿namespace Zygor_Client.UserControls
{
    partial class HomeView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        public System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.accountOverview = new AccountOverview();
            this.productsView = new ProductsView();
            this.SuspendLayout();
            // 
            // accountOverview
            // 
            this.accountOverview.Dock = System.Windows.Forms.DockStyle.Left;
            this.accountOverview.Location = new System.Drawing.Point(0, 0);
            this.accountOverview.Margin = new System.Windows.Forms.Padding(0);
            this.accountOverview.Name = "accountOverview";
            this.accountOverview.Size = new System.Drawing.Size(280, 508);
            this.accountOverview.TabIndex = 3;
            // 
            // productsView
            // 
            this.productsView.Dock = System.Windows.Forms.DockStyle.Left;
            this.productsView.Location = new System.Drawing.Point(280, 0);
            this.productsView.Margin = new System.Windows.Forms.Padding(0);
            this.productsView.Name = "productsView";
            this.productsView.Size = new System.Drawing.Size(820, 508);
            this.productsView.TabIndex = 4;
            // 
            // HomeView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.Controls.Add(this.productsView);
            this.Controls.Add(this.accountOverview);
            this.Margin = new System.Windows.Forms.Padding(0);
            this.Name = "HomeView";
            this.Size = new System.Drawing.Size(1100, 508);
            this.ResumeLayout(false);

        }

        #endregion
        public AccountOverview accountOverview;
        private ProductsView productsView;
    }
}
