﻿using System;
using System.Windows.Forms;
using Zygor_Client.Util;

namespace Zygor_Client.UserControls
{
    partial class SettingsTab
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.settingsTabLabel = new Zygor_Client.CustomControls.SettingsTabLabel();
            this.SuspendLayout();
            // 
            // settingsTabLabel
            // 
            this.settingsTabLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.settingsTabLabel.Font = new System.Drawing.Font("Arial", 11F);
            this.settingsTabLabel.Location = new System.Drawing.Point(0, 0);
            this.settingsTabLabel.Name = "settingsTabLabel";
            this.settingsTabLabel.Size = new System.Drawing.Size(280, 38);
            this.settingsTabLabel.TabIndex = 0;
            this.settingsTabLabel.Text = "General";
            this.settingsTabLabel.Click += new System.EventHandler(this.settingsTabLabel_Click);
            this.settingsTabLabel.MouseEnter += new System.EventHandler(this.SettingsTab_MouseEnter);
            this.settingsTabLabel.MouseLeave += new System.EventHandler(this.SettingsTab_MouseLeave);
            // 
            // SettingsTab
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.Controls.Add(this.settingsTabLabel);
            this.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.Margin = new System.Windows.Forms.Padding(0);
            this.Name = "SettingsTab";
            this.Size = new System.Drawing.Size(280, 38);
            this.ResumeLayout(false);

        }

        #endregion

        public CustomControls.SettingsTabLabel settingsTabLabel;
    }
}
