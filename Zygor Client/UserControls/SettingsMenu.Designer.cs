﻿namespace Zygor_Client.UserControls
{
    partial class SettingsMenu
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.settingsContainerLeft = new System.Windows.Forms.FlowLayoutPanel();
            this.SuspendLayout();
            // 
            // settingsContainerLeft
            // 
            this.settingsContainerLeft.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.settingsContainerLeft.Dock = System.Windows.Forms.DockStyle.Fill;
            this.settingsContainerLeft.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.settingsContainerLeft.Location = new System.Drawing.Point(0, 0);
            this.settingsContainerLeft.Margin = new System.Windows.Forms.Padding(0);
            this.settingsContainerLeft.Name = "settingsContainerLeft";
            this.settingsContainerLeft.Size = new System.Drawing.Size(280, 508);
            this.settingsContainerLeft.TabIndex = 5;
            // 
            // SettingsMenu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.Controls.Add(this.settingsContainerLeft);
            this.Margin = new System.Windows.Forms.Padding(0);
            this.Name = "SettingsMenu";
            this.Size = new System.Drawing.Size(280, 508);
            this.ResumeLayout(false);

        }

        #endregion

        public System.Windows.Forms.FlowLayoutPanel settingsContainerLeft;
    }
}
