﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using Zygor_Client.Events;
using Zygor_Client.Util;

namespace Zygor_Client.CustomControls
{
    public partial class ZScrollBar : Panel
    {

        public event EventHandler<ScrollEventArgs> ScrollbarMoved;

        public int _thumbY = 0;
        private int _thumbHeight;

        private volatile bool _sliding = false;
        private bool _hovered = false;
        private int _pressedY;
        private bool _pressed = false;

        public ZScrollBar()
        {
            InitializeComponent();

            MouseMove += OnMouseMove;
            MouseLeave += OnMouseLeave;
            MouseClick += OnMouseClick;
            MouseDown += delegate(object sender, MouseEventArgs args) { _pressedY = args.Y; _pressed = true; };
            MouseUp += delegate(object sender, MouseEventArgs args) { _pressed = false; };

            SetStyle(
                ControlStyles.UserPaint |
                ControlStyles.DoubleBuffer | 
                ControlStyles.AllPaintingInWmPaint, true);
        }

        private void SetInitialSize(object sender, EventArgs e)
        {
            SetThumbHeight(Height);
        }

        public void SetThumbHeight(int height)
        {
            _thumbHeight = height;
            if (InvokeRequired)
                Invoke(new MethodInvoker(Refresh));
            else
                Refresh();
        }

        private void SetThumbPosition(int target)
        {

            var min = 0;
            var max = Height - _thumbHeight;
            target = Math.Max(min, target);
            target = Math.Min(max, target);
            _thumbY = target;

            if (this.InvokeRequired)
                Invoke(new MethodInvoker(Refresh));
            else
                Refresh();

            if (ScrollbarMoved != null)
            {
                var args = new ScrollEventArgs(ScrollEventType.EndScroll, target);
                ScrollbarMoved(this, args);
            }
        }

        private void SlideThumb(int direction)
        {
            if (_sliding)
                return;

            int target = Math.Max(0, Math.Min(Bottom - _thumbHeight, _thumbY + (50 * direction)));
            

            _sliding = true;
            new Thread(() =>
            {
                var rand = new Random();
                while (_thumbY != target)
                {
                    var dy = target - _thumbY;

                    SetThumbPosition(_thumbY + Math.Min(dy, 1));
                    Invoke(new MethodInvoker(Refresh));
                    if (rand.NextDouble() > 0.75)
                        Thread.Sleep(1);
                }
                _sliding = false;
            }).Start();

        }

        private void OnMouseClick(object sender, MouseEventArgs e)
        {
            var loc = e.Location;
            if (!IsMouseOnThumb(loc))
            {
                var dy = loc.Y - _thumbY;
                if (dy != 0)
                    SlideThumb(Math.Sign(dy));
            }
        }

        private void OnMouseLeave(object sender, EventArgs e)
        {
            _hovered = false;
            Refresh();
        }

        private void OnMouseMove(object sender, MouseEventArgs e)
        {
            var loc = e.Location;
            var prev = _hovered;
            _hovered = IsMouseOnThumb(loc);
            if (prev != _hovered)
                Refresh();

            if (_pressed && e.Button == MouseButtons.Left)
            {
                var dy = loc.Y - _pressedY;
                SetThumbPosition(_thumbY + dy);
                _pressedY = loc.Y;
                Invoke(new MethodInvoker(Refresh));
            }
        }

        private bool IsMouseOnThumb(Point location)
        {
            return location.Y >= _thumbY && location.Y <= _thumbY + _thumbHeight;
        }

        public override void Refresh()
        {
            Visible = _thumbHeight < Height;
            base.Refresh();
        }

        protected override void OnPaint(PaintEventArgs pe)
        {
            var graphics = pe.Graphics;
            graphics.Clear(TrackColor);
            graphics.FillRectangle(_hovered ? BrushColorHovered : BrushColorNormal, 0, _thumbY, Width, _thumbHeight);
            
        }
    }
}
